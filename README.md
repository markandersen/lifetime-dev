# VPLifetimes POC

## Purpose

The purpose of the model is to provide estimates of future customer behavior and value from past data.  
This particular data product leverages the [Buy-Til-You-Die (BTYD)](https://en.wikipedia.org/wiki/Buy_Till_you_Die) lineage of work most recently 
manifest in [Peter Fader and Bruce Hardie’s papers](http://brucehardie.com/papers/018/fader_et_al_mksc_05.pdf), and the 
[Lifetimes python module by Cameron Davidson-Pilon](https://lifetimes.readthedocs.io/en/latest/index.html). 
 BTYD is a model for estimating lifetime value for customers which are engaged in *non-subscription* purchases, a type of transaction applicable for over 90% of Vistaprint transactions.

The uses of scores from the VPLifetimes model include:
* Ranking customers on relative value
* Estimating residual (future) Lifetime Value (LTV) for a customer
* Estimating probability a customer will make any future purchase
* Predicting number of orders a customer will make in a future time period (90, 180 365 days)
* Predicting Adjusted Gross Profit (AGP) for a customer in a future time period
* Estimating AGP for a customer order-date if they were to make a purchase (“conditional expected value”).

### Glossary 

**Customer** this term is used synonymously with shopper: a single end-user identity.

**Order** refers to a time interval in which one or more items are purchased
by a customer with a net positive Adjusted Gross Profit (AGP).  By grouping together items which
may span multiple transactions in a given time period (typically a day), this approach avoids letting
marketing artifacts such as coupons, discounts, and shipping options obscure the regularity with 
which customer's purchase.  Interactions which are not AGP-positive are excluded, as required by 
the gamma-gamma monetary model, and thus are treated as non-events.

**Lifetime Value (LTV)** refers to residual (forward looking) cumulative value for a customer, computed
 as the three year estimated cumulative Adjusted Gross Profit (AGP).  This residual LTV is *not discounted,
  does not look beyond three years, and does not include the value of past transactions*.

**Adjusted Gross Profit (AGP)** is calculated as bookings - variable and fixed product costs - support costs (GSO, 
PSO, COGS).  AGP is used as the *monetary value* field in Lifetimes.  A noteworthy limitation is that
Lifetimes's gamma-gamma distribution only allows for positive monetary values, and we have approached this limitation
by removing non-positive AGP orders (and order dates) from the data used for fitting and scoring.

**Modified Beta Geometric (MBG)** is one of the models provided by Lifetimes.  It has proven to be more efficient
and accurate than other Lifetimes models and is heavily used in this library.  Beta Geometric (BG) is an alternative
and slightly less accurate model.

**Gamma-Gamma (GG)** monetary model is the single monetary model provided by Lifetimes.  It is applied to AGP 
and assumes all transactions have positive AGP values.

## Consumer (of the Data Product) Perspective

### Getting Shopper Scores For a Country

The data product is a Snowflake table with scores for each shopper.  1.3 million records are currently scored for IE and 
ES with each daily run.  Only these countries are currently run (pending data availability).

Querying to get the most recent scores for each shopper in a country is simple:
```
set country='ES';
select * from staging.dna.vplt_shopper_nondigital where
model_id = 
(select model_id From staging.dna.vplt_model where countries=$country
and score_timestamp = (select max(score_timestamp) 
from staging.dna.vplt_model where countries=$country))
```

The query above will find all the models specific to a country in the vplt_model_nondigital
table and get the id for the most recent one, and then get the shopper scores associated 
with that model.

One can get scores from earlier models and compare.

At present, scoring exists for shoppers in IE and ES for whom their first order 
is on or after 2010-07-01.  Shoppers which are 
anonymous or for whom the first order is before that time do not yet have scores.

| field | definition |
| :--- | :--- |
| shopper_id | Identifier for shopper |
| model_id | Which model in the vplt_model table was used to score shopper. |
| frequency | Frequency value which was assumed for shopper when calculating scores.  Note: Lifetimes frequency excludes first transaction. |
| recency | Recency value which was assumed for shopper when calculating scores. |
| t | T value assumed for shopper when calculating scores. |
| monetary_value | AGP monetary value assumed for shopper when calculating scores. |
| prob_alive | Score for probability customer is alive. |
| conditional_expected_value_per_order | Score for AGP conditional on customer placing an order. |
| fcst_orders_90 | Score for number of orders in next 90 days. |
| fcst_value_90 | Score for AGP in next 90 days ( = fcst_order_90 * conditional_expected_value). |
| fcst_orders_180 | Score for number of orders in next 180 days. |
| fcst_value_180 | Score for AGP in next 90 days ( = fcst_order_180 * conditional_expected_value). |
| fcst_orders_365 | Score for number of orders in next 365 days. |
| fcst_value_365 | Score for AGP in next 90 days ( = fcst_order_365 * conditional_expected_value). |
| score_timestamp | When shopper score was created. |

*Frequency, recency, and T are all specific to Lifetimes and to be understood in that context rather 
than directly as RFM values.  See glossary to review definition of order.*

### Inspecting the Prediction Model

The Snowflake model table (vplt_model) retains the parameters which were used to generate
shopper scores.  These parameters can assist in interpreting changes in scores over time
or across countries.

This query will return information on the prediction model type in the field 'predict_model_type', 
prediction parameters in 'predict_params', 
and the gamma-gamma monetary model parameters in 'gamma_params':
```
set country='ES';
select * From staging.dna.vplt_model where countries=$country
and score_timestamp = (select max(score_timestamp) from staging.dna.vplt_model where countries=$country)
```

| field | definition |
| :--- | :--- |
| model_id | Unique identifier for model which was run on a particular day.  Joining key for scores. |
| countries | Countries which were included in the model run.|
| predict_model_type | Type of prediction model which was used (ModifiedBetaGeo or BetaGeo). |
| predict_params | Dictionary of parameters specific to the prediction model type. |
| gamma_params | Gamma-Gamma model parameters. |
| score_timestamp | When the model was run. |

For example, in a given row one could exxamine the predict_params column to find
a dictionary which describes
the values of a, alpha, b and r which were used when running the MBG model:

```
{
  "a": 1.29718,
  "alpha": 311.881,
  "b": 0.92665,
  "r": 1.1062
}
```

### How Accurate is VPLifetimes? 

This question can be answered in terms of business measures and statistical measures.

Moreover, accuracy can be measured with *absolute* accuracy measurements (RMSE and MAE) and *relative* accuracy
measurements (RSpearman).  The end user should consider whether their goals are absolute or relative when 
evaluating VPLifetimes for use.

Calculating statistical metrics involves modeling a portion of the period (calibration) and making predictions
for the remaining period (holdout).  The following section describes metrics which were used to assess accuracy.
So these accuracy metrics should be understood to measure how predictive a model would have been if used in the
past for a past period.

Recent discontinuities include: Covid-19 and new site launches.  

Model training during the calibration period determines the model coefficients.  A discontinuity within the training
period may impact model training.

Accuracy estimation during the holdout period is done by applying the trained model to this holdout period.
Discontinuities in the holdout period may impact accuracy.

Since the current models are trained over a larger period (2010-2020) that includes both site launches and Covid-19
to various degrees, the effect on prediction for late 2020-2022 is not yet known.

#### RMSE / MAE 

Root Mean Squared Error (RMSE) and Mean Absolute Error (MAE) are generally accepted metrics for 
measuring the *absolute* accuracy of predictions.
* Does the model correctly estimate the probability of order: P(order)?
* Does the model correctly estimate spend in a future period: residual LTV?

**RMSE** measures the squared difference between predictions and ground truth (holdout).
It penalizes wild predictions heavily.  A smaller value is preferred to a larger value.

**MAE** measures the absolute difference between predictions and ground turth (holdout).
It penalizes all errors in proportion to absolute error size.  A smaller value is preferred to a
larger value.

Visual comparisons (further below) may seem more informative than absolute values for these
metrics.  One can think about sample predictions to understand RMSE and MAE.  
One should also compare with the *zero-model* which is an alternative model where
we assume no customer will order again.  If the RMSE and MAE are not lower than the zero
model it suggests the model is not much more accurate than a naive assumption.

These figures are based on calibration during 2010-07-01 through 2019-09-30 and holdout
2019-10-01 through 2020-08-01.  Note the holdout period includes both site launch and Covid-19.

| country | model_type | frequency | #Orders RMSE | #Orders MAE  | Predicted Orders | Actual Orders | Predicted AGP | Actual AGP |
| :--- | :---: | :---: | ---: | ---: | ---: | ---: | ---: |---: |
| IE | MBG-GG | Daily | 0.32 | 0.12  | 31,889 | 20,876 | $1,045,106 | $516,218 |
| IE | Zero-Model | NA | 0.36 | 0.06 | 0 | 20,876 | $0 | $516,218 |
| ES | MBG-GG | Daily | 0.30 | 0.10  | 68,365 | 59,959 | $1,879,334 | $1,358,360 |
| ES | Zero-Model | NA | 0.37 | 0.07 | 0 | 59,959 | $0 | $1,358,360 |

In both models, the RMSE beats the zero model by about 10% but the MAE is inferior.
This reflects the fact that few customers placed repeat orders in the last year, and the zero model
is a good basis if one is not penalizing squared errors.  However, the failure of the zero model
to predict high volume customers is reflecting in its lower performance on RMSE.

The predicted orders and AGP give a clear indication of how limited the utility of a zero model 
would be for aggregate measures.  Again, the depressing effect of Covid-19 may account for some of 
the inaccurate holdout.

In practice, most variation in predictions arises from the P(order) model as compared with the monetary value 
prediction.  

#### Spearman's R 

Spearman's R (rank correlation) measures whether VPLifeimes ranks customers, a 
measure of *relative accuracy* across customers.  For many operational purposes, such as targeting
a subset of customers, ranking customers may be more important than estimating their absolute probability 
of order.

Higher values of Spearman's R are preferred to lower values.  A zero-model is not reported, because
a zero model automatically has a Spearman R of 0.0.  A negative Spearman R value would indicate 
a substantially incorrect ranking.  Values like 0.2-0.4 might indicate weak, but useful relationships.
Values above 0.4 would indicate much stronger relationships.

Since most variation for Vistaprint customers is driven by whether an order occurs, rather than 
the amount spent on an order, it is critical to predict Prob(Order).  The challenge for rank correlation
is that most customers have 0 orders, and there is no rank distinction among those.  This requires that
Spearman R properly handle ties.  

Estimating Spearman R for Est(#Orders) also has massive ties at 1 and 2 orders, so the rank correlation
model is limited by making a correlation between a continuous prediction Est(# Orders) and discrete
ground truth (# Orders) which largely has the values 0, 1 or 2 at tied ranks.

Estimating Spearman R for AGP is only slightly better.  The customers with 0 orders all have 0 AGP, so the largest
group has tied values.  But for those customers with 1 order, their value is fanned out in a small range
based on AGP for that order.  In this case, a high correlation would mean that VPLifetimes is 
successfully placing the 0 order customers lowest, and then tending to rank on AGP basis those customers
who have placed orders in the holdout period.

* August 3 2020 Results using MBG model
  * (Calibration: 2010-07-01 - 2019-09-30)
  * (Holdout: 2019-10-01 - 2020-08-01)

| country | model_type | frequency | SpearmanR for Est(#Orders) | SpearmanR for AGP |
| :--- | :--- | :--- | ---: | ---: |
| IE | MBG-GG | Daily   | 0.239 | 0.237 |
| ES | MBG-GG |Daily    | 0.305 | 0.303 |

* Conclusions:
  * For ES, the Spearman R ~= 0.30 indicates material correlation, which is also
   demonstrated in example charts (below).  This indicates a modest predictive relationship, 
   so customers with higher model scores are 
  expected to place more orders and with more AGP than those with lower scores.
  * For IE, the Spearman R ~= 0.24 indicates a weaker correlation with some predictive value,
  greater for the top decile.
  * Both models substantially outperform the zero model (no information).

#### Visualization of Holdout Fit Metrics

This library provides visualization of calibration-and-holdout for detailed understanding of how lifetimes functions.
Typically data scientists focus on key metrics, such as RMSE, MAE, and SpearmanR which are used to drive optimization and choose best models.  However, as statistics, these metrics fail to tell a story about the distributions in samples and models which are important to ask questions such as *for which customers* does the model function poorly or well.  The related visualizations fill in this gap, giving the data scientist insight into how the model is functioning when test against holdouts.

This first chart helps understand the extent to which predictive value spans different levels of customer engagement:

![holdout analysis](docs/source/ES-calend-2019-09-30-obs-2020-08-01-model-ModifiedBetaGeo-rank.png)

Interpretation helps understand how predictive performance relates to different segments of customers.
* Top three charts pertain to Exp(#Orders)
  * Chart 1: Customers predicted as worst (0) to best (800K+) left to right.  Blue jittered points are number of orders in holdout period, typically 0, 1, 2 or 3.  Black is the predicted number of orders, which are continuous typically 0...2.0+.  Chart shows increasing density of non-zero values among customers predicted best, and Spearman R indicating that ranking was correlated with actual holdout orders.
  * Chart 2: Customers predicted from worst to best, put into 20 equal size buckets.  For each bucket, the actual number of orders is compared with the predicted number of orders.  Close alignment of lines indicates reasonable model, and prediction above actual indicates that model overstates expected performance.
  * Chart 3: For cumulative plot, customer order is reversed from best (800K+) left to worst(0) on right.  Inflection point indicates some actual customers performed higher than expected.  Lower orange line indicates gap in total predicted orders between model and actual.
* Bottom three charts pertain to AGP:
  * Chart 4: Customers predicted as worst (0) to best (800K+) left to right.  Blue jittered points are AGP (jittered because of the preponderance of 0's).  Increasing value indicates that model predicts higher AGP successfully for more highly ranked customers.  Spearman R shows degree of correlation between predication and AGP outcome.
  * Chart 5: Customers predicated from worst to best, put into 20 equal size buckets.  Actual spend is compared with model spend.  Relationship is visible.  Predicted spend somewhat above actual spend for highest buckets, likely driven by predicted v. actual orders.
  * Chart 6: For cumulative plot, customer order is reversed from best (800K+) left to worst(0) on right.  Gap between lines indicates extent to which model predicts aggregate performance above actual.

Charts in this viz format can be generated for calibration-holdout sets using VPLifetimesHoldoutPlots.plot_model_rank_vs._holdout().

This second chart provides a sense of whether the model successfully distinguishes buyers from non-buyers:

![holdout analysis](docs/source/ES-calend-2019-09-30-obs-2020-08-01-model-ModifiedBetaGeo-frequency.png)

Intepretation helps relate model predictions to outcomes in the holdout data set.

The data for customers with actual holdout orders = (0, 1, 2, 3, 4, 5) are each segregated into separate charts, to understand what the model predicted for these customers.  While we should understand acutal holdout orders as itself a random sample from a distribution, reverse engineering in this manner gives one a sense of whether the model treated customers differently with different outcomes.  If the charts looked visually much alike, it indicates that the model has relatively low power.

Each chart includes the following elements:
* Subtitle indicating how many orders were placed by customers in this band (0,...,5)
* A kernel density estimation (KDE) plot with a solid blue line which shows the expected distribution if a large number of customers had existed.
* Histograms showing the actual number of customers predicted in each (fractional) bucket by the model.  Buckets up to the median have red color and above the median are blue, to provide a visual indication of the median.  Due to the long tail, the mean is always higher than the median and is shown with a dotted line to the right of the median brak.  Again, seeing increasing means and medians for customers with higher actual performance is qualitative confirmation the model is working as expected, and doing so across a range of customers.

A limitation of these charts is that most customers exist in the top couple charts, and this is not effectively communicated.

Charts in this viz format can be generated for calibration-holdout sets using VPLifetimesHoldoutPlots.plot_holdout_frequency_charts().


#### Ireland New Site and Prediction Accuracy

Vistaprint's new website for Ireland launched on 2019-09-04.  As a consequence of post-launch business
performance and data flows, greater discontinuity exists for Ireland in this data set than for Spain.

These are the underlying Ireland trends which drive models:

![time-to-order](docs/source/ie_monthly_agp.png)

While VPLifetimes model accuracy is somewhat lower for Ireland (September 2019-July 2020), the model works at 
expected levels in earlier periods. 

### How Accurate Should Lifetimes Be?

As data science models are not crystal balls, it should not surprise the end user that these estimates 
have limited accuracy.  In some cases, there may be more accuracy in the aggregate than for 
particular shoppers.  We have attempted to create insights into the types of accuracy issues to 
be expected, and how these estimates may best be used in practice.  Understanding what this model does
 and does not do may also dispel some of the mystery and allow the end user to understand the best way to use these models.

In data science, one should always think about the baseline or “null hypothesis”, which describes what you
 would believe or use in the absence of a specific model.  This model fundamentally combines two independent
  models: one model for probability the customer is alive (and the accompanying expected number of future purchases)
   and another model for expected future spend.  Each of these sub-models have their own null hypothesis as follows:

|| Lifetimes Model|Naive (Null) Model|
| --- | --- | --- |
|Probability (Alive) (P-Alive)|Varies from 0% to 100%, depending on past purchase frequency|0%|
|Expected Future Profit Per Order (Exp-Spend)|Varies from $0+ based on a weighted average of person’s history and a collective average, where the individual weight is driven by number of order-dates|Mean value for the cohort from which the shopper is drawn (e.g. mean value for the country)|

Throughout this discussion we will discuss these two variables: P-Alive and Exp-Spend.

### Business Metrics

End users typically prefer measures which are directly drawn from the underlying model.  
There are subtleties in applying these concepts to business problems, and the accompanying commentary 
is intended to help end users think through these challenges.

| Business Concept | Metric | Computation | Commentary |
| --- | --- | --- | --- |
| Should I market to this customer? | P(Alive) | | Customers which have a low P(Alive) would seem dead.  Lifetimes' P(Alive) is measured absent a concerted marketing effort, so customers which have low P(Alive) have virtually no value *without* marketing effort, while those with high P(Alive) might not require the same level of marketing effort to reactivate. |
| How much should I spend to market to this customer? | Lifetime Value | | Lifetimes' LTV estimates the residual LTV of a customer absent any particular marketing effort, so while it might seem to be a cap on marketing spend, it would not be if the marketing spend had the effect of bringing customers back and thereby increasing their lifetime value. |
| Have my actions raised the value of one or more customers? | Lifetime Value | P-Alive * Exp-Spend | Comparing a customer before and after a a marketing event would be one way to estimate LTV effects.  However, as the LTV effect is mainly driven by whether a customer places an order, this may be a more complex and indirect way to estimate a marketing effect.  Furthermore, uplift should be measured to avoid *cannibalization*, for example a marketing effort could accelerate an order a customer already plans to make.  From a non-discounted, uplift perspective no value has been created.  In fact, after the order, the residual LTV for the customer could be dramatically reduced.  The Lifetimes model is driven by data showing the customer is likely to be alive, driving up the customer residual LTV at exactly the moment after the customer has fulfilled their needs by placing an order.|
| How many orders will this customer place in the next year? | Exp-Orders | Derived from P-Alive | | 

In forecasting models, predictions for future behavior are based on past behavior.  Therefore, 
changes in marketing campaigns, products, and product offers can reduce the accuracy of a forecast.  
We should understand these models to predict what would happen if the past continues and nothing changes.

Experiments are always preferred to passive observational models, and marketing teams are encouraged to 
combine these and other data product to test hypotheses and demonstrate causal realtionships.

Business interventions which translate into heightened customer engagement will be quickly reflected as 
added value.  This gives us hope that we can measure whether our marketing activities have a meaningful 
impact on customer value (at least at a group level).  One caveat is that the model would not know about 
cannibalization, so if marketing activities merely accelerate or swap purchases which would already occur, 
then the model may overstate the value of those activities until the resulting future deprivation is also 
experienced (and the model will not understand inter-temporal swaps correctly).

*Note: A CLV model exists for customers after placing their first order, and end users 
may prefer to use those valuations for customers until a second order is placed. 
 However, mixing models will result in values which are not aligned.*

### Time to Second and Third Order

VPLifetimes provides tools to understand the business context in which it operates.  Among the most important operating
metrics are % of customers who place multiple orders, time to second order, and time to third order.

With a dataframe of distinct orders dates per shopper, one can construct TimeToOrderPlots such as this example
for the ES business:

![time-to-order](docs/source/time-to-second-order-ES.png)

These subplots include:
* Fraction of customers with 1, 2, 3, 4... orders
* Number of years to second order date, which shows recurrent peaks around 1 year after initial order date
* Days to Second order and Days to Third Order.  These charts show that as much as 30% of customers
take more than one year to place their second order, and two years to place their third order.  These
metrics underline the need to do Lifetimes analysis with data over longer timeframes such as 5+ years
* Comparing time to second order date with time to third order date, a scatter plot shows intensities
around annual intervals, suggesting an annual cycle for significant customers (even if skipping certain
years).  Annual order cycles is a degree of regularity inconsistent with assumptions of the 
BTYD framework, and ideally these customers will be segmented out
* The bottom charts confirm expectations, that the reorder subpeak (for those not re-ordering within
the first half-year) occurs at around the one year anniversary and that these occurrences correspond
to the Vistaprint holiday season.

Conclusions from these analyses suggest that partitioning customers into holiday and non-holiday
customers will be important potential steps.  This partition may involve more than date, but also
product ordered.

### Identifying the Best Customers With Time to Second Order

VPLifetimes provides visualizations to compare time to second order for shoppers which have
placed different numbers of orders.  These charts allow us to better understand:
* How long does it take for Vistaprint's best customers to manifest themselves
* The timeframes required for Lifetimes analysis for the best customers

Below is the chart based on ES customers over 2010-2020:

![time-to-order](docs/source/n_order_shoppers_days_to_second_ES.png)

*There is small survival censorship which may occur with these charts, but it is not expected
to bias the results materially.*

From this chart, we can see that 50% of order customers place their second order at 194 days, and 80% at 
561 days.  More interesting would be the behavior of better shoppers, and here for example, we could
examine shoppers who have ordered on six (6) distinct order dates.  50% of these shoppers have placed
their second order by day 98, and 80% by day 363.

From this, we can conclude:
* Solid, repeat customers do not disclose quickly.  In half the cases one has to wait for over one
quarter after initial order date, and in another 30% of cases one has to wait up to one year.
* After one year, one has a pretty good bet on which customers have some chance of being higher value,
although it does not catch all customers.
* A full half of the best customers place no follow up orders within a quarter after initial order,
and therefore order behavior during that quarter is a limited tool for ultimate value.


### Splitting Holiday and Non-Holiday Shoppers

The annual patterns identified above for holiday shoppers indicate that these shoppers
have very different patterns from the majority of Vistaprint shoppers.

A first step is to use data to separate holiday and non-holiday shoppers.  A second
step will be to estimate VPLifetimes separately for each subgroup and determine if the
combined model is more accurate than a single overall model.

Based on the chart above and business knowledge, a first proposal is to identify customers
as belonging to a segment "first order holiday shoppers" if their first order is in November or
December and it is from a likely holiday product category (Consumer, PPAG Hard Goods,
PPAG Clothing & Bags, or Holiday).

Splitting customers which are "first order holiday shoppers" from others we repeat the visualizations
and determine that isolation is fairly effective:

![es-time-holiday](docs/source/time-to-second-order-ES-Holiday.png)

![es-time-non-holiday](docs/source/time-to-second-order-ES-Non-Holiday.png)

Comparing the charts, one can see the holiday chart has the strong annual repeat clusters
on the scatter chart, and the strong upward annual inflections each year.  The other shoppers
have neither pattern as their shopping propensity is not as strongly driven by annual cycles.

Splitting customers into holiday and non-holiday shoppers may increase accuracy within the 
holiday segment, but does not materially change the overall accuracy.  The split does provide
an interesting perspective that predicted orders are too high for the non-holiday segment and 
too low for the non-holiday segment, and when combined into a composite model the overall
prediction runs high (most customers are non-holiday).

Selective results with v0.14 data for IE and ES:

| country | cohort | RMSE exp(orders) | Spearman R exp(orders) | RMSE AGP | Spearman R AGP |
| :--- | :--- | ---: | ---: |---: |---: | 
| IE | base |        0.324 | 0.24 | 14.63 | 0.24 |
| IE | combined |    0.324 | 0.24 | 14.67 | 0.24 |
| IE | non-holiday | 0.335 | 0.23 | 15.79 | 0.23 |
| IE | holiday |     0.281 | 0.27 |  9.25 | 0.27 |
| ES | base |        0.298 | 0.30 | 10.29 | 0.30 |
| ES | combined |    0.297 | 0.30 | 10.28 | 0.30 |

*Base=single model, combined=composite of non-holiday and holiday. Further insight can be found in holiday_split.ipynb.*

Given similarity of results, there is no immediate need to model holiday and non-holiday customers separately
with VPLifetimes.

## POC Operations Perspective

### Databricks Runtime

The production POC runs on Databricks https://dbc-dna-cbp-dev.cloud.databricks.com/ in 
notebook mandersen@vistaprint.com/Lifetimes/_process.  This job runs at 6:00 am PDT.
  * Countries: IE and ES (parameters of notebook)
  * Shoppers with first order >= 2010-07-01 (hardcoded in notebook)
  * Orders will span up to one day before run date.
  * Parameters for MGB and GG models per country (hardcoded in notebook)
  * Model_id is generated uniquely each day.  It has no semantic value.

It is **important** that shoppers are re-scored daily.  
The Lifetimes model applies a gradual decay to non-purchasing shoppers each time interval, 
and whenever a new purchase occurs the model dramatically changes the shopper's Prob-Alive and residual LTV.  
Incorporating recent orders into the data is required to keep customer scores up to date.

## Library Developer Perspective

### Development and Analysis

* Two classes are used primarily in the Spark environment for reading and writing files
  * VPLifetimesData -- reads data from Snowflake and saves as parquet or pandas
  * VPLifetimesScoreWriter -- calls scoring method on shoppers and writes data to Snowflake
Both of these files exist primarily for use in Databricks.  These files are integrated into git
by downloading as .dbc archive files and simply updating the git repo.

#### Merging Snowflake Shopper and Order Data 

A dataset combining shoppers and orders is required to run this process.  Since lifetimes requires multiple years for training, this product requires data spanning both the monolith and new platform.  The data itself is under flux, as the team's engineers are trying to pull this together.  As a result, the initial POC only works for IE and ES, and even those results may change when the shopper cohorts are redone.

Recently, the vistaprint.transactions.ordered_info_all table was created to provide an integrated view of orders.

![holdout analysis](docs/source/LTDataDetails.png)

The VPLifetimes algorithm first identifies eligible shoppers, which are those associated 
with the country of interest and which have a cohort falling within the date range.  
Then all the orders for those shoppers are obtained from ordered_info_all.  
Since Lifetimes treats an order as all those that occur during a period (such as day), 
and negative monetary values are not permitted, those orders are screened from the set of orders, 
and then summed up into the total for a day.  Moreover, subscription orders should be excluded from the BTYD 
framework, and these orders are removed at this point.  (The intent is to add back digital LTV value separately at a later stage.)

##### Data Corrections

A great many records from the new platform have NULL values for Gross Profit.

The VPLifetimes code was adjusted to be robust to NULL GP values.  The process was to review ES and IE data
which did not have NULL GP and calculate the ratio between product and shipment GP and bookings.  For records with
NULL GP values, the view lifetimes_order_dates corrects for NULLs using hard-coded values based on these measurements:

```
create or replace view sandbox.cbpm.lifetimes_order_dates copy grants as 
select order_shopper_id, order_date, sum(total_bookings_usd) as total_bookings_usd,
sum(agp_budget_usd) as agp_budget_usd,
category,
product_name,
channel from
(select order_shopper_id,
to_date(order_created_datetime) as order_date, 
total_bookings_usd,
ifnull(product_variable_gp_budget_usd, .784*total_bookings_usd) + 
ifnull(shipping_gp_budget_usd, -.016*total_bookings_usd) -
ifnull(gso_cogs_usd, 0) - 
ifnull(pso_cogs_usd, 0) - 
ifnull(care_cogs_usd, 1.262) - 
ifnull(fixed_product_cost_usd, 0) as agp_budget_usd,
category,
product_name,
channel
from vistaprint.transactions.ordered_item_info_all items
where 
ifnull(items.monolith_is_customer_care_flag,0) = 0 
and ifnull(items.monolith_is_replacement_order_flag,0) = 0 
and ifnull(items.monolith_is_recurring_flag, 0) = 0 
and ifnull(items.monolith_is_digital_flag, 0) = 0 
and ifnull(items.monolith_promospot_order_flag, 0) = 0 
and ifnull(items.website, '') != 'DIGI' 
and items.total_bookings_usd > 0 
) non_digi
group by non_digi.order_shopper_id, non_digi.order_date, category, product_name, channel
having sum(non_digi.agp_budget_usd) > 0
```

The two magic number 0.784 and -.016 were arrived at by running queries on 2020-08-04 for ES and IE.
The values returned were almost identical for the two countries, and the slightly more conservative
ES values were used as global constants in this version of the procedure.  

Here is a sample query used to generate constants:
```
select sum(total_bookings_usd) bk_total, sum(product_variable_gp_budget_usd) prod_total,
avg(total_bookings_usd) bkn_average, avg(product_variable_gp_budget_usd), sum(product_variable_gp_budget_usd) / sum(total_bookings_usd) as ratio
from vistaprint.transactions.ordered_item_info_all
where country = 'ES' and total_bookings_usd is not null and product_variable_gp_budget_usd is not null
and total_bookings_usd > 0
and order_created_datetime > '2020-04-07'
```

*The ES date was based on site launch, and 2019-09-04 was used for the IE query.*

##### Downloading data from Snowflake


Below is example code which can be used in Spark to download sample 
shopper-order data (using the method above) and write out as a pandas pickle file.  The pickle file can be moved to a client machine for analysis or scoring with this library.

```
import uuid
cohort_start_date = '2010-07-01'
cohort_end_date = '2020-07-21'

# Illustrations: Download one file for IE, one file for ES, and a combined file for both
for countries in ['IE', 'ES', ['IE', 'ES']]:  
  print(f'Importing for {countries}')
  model_id = str(uuid.uuid4())  
  vp_lifetime_data = VPLifetimesData(model_id, 
                                  countries=countries, 
                                  sample_start_date=cohort_start_date, 
                                  sample_end_date=cohort_end_date)
  vp_lifetime_data.get_orders(debug=True)
  vp_lifetime_data.pickle(file_type='pandas') 
```

#### Moving Datasets Locally 

Since the data files for the project are created in predictable locations, these databricks-cli can be transferred locally 
with commands such as the following which can list available files and transfer them locally:

```
databricks fs ls dbfs:/FileStore/VPLifetime/pickle
dbfs cp dbfs:/FileStore/VPLifetime/pickle/<filename> .
```
In the repo, it is advisable to copy the files to /data/01_raw as a standard location for use.


#### Model Fitting

The Lifetimes model is currently fit against an entire data set rather than just the calibration period.

Repeated analysis finds that as long as 3+ years of data is used for fitting, the results are reasonably stable.  Removing the holdout period will not typically change the results of the model.

Since the model is parametric, the values of the parameters can be reviewed against other types of runs.  
Relevant findings (based on analysis of IE and ES only):
* Parameters are largely stable
* Business conclusions are not highly sensitive to small perturbations in parameter values
* Parameter values differ materially across countries

* Run final_model_train.py to train the models.
  * This will produce training scores for IE and ES.
  * Code is hard-coded for desired start-date, end-date and source data version
  * The model writes a csv file in data/07_model_output with the results of fitting various models.
  * If the developer wishes to use these parameters they can copy these to the production process.
  * These parameters can also be contrasted with those used in the calibration and holdout exercise.  

Model fitting should be run quarterly to ensure repeatability.  Since parameters are largely stable, there is no need to refit the simple Lifetimes models more regularly.

##### Sample Parameters

The Lifetimes models are parametric: typically a beta-geometric (BG) or modified-beta-geometric
model wih 4 parameters combined with a gamma-gamma spend model with 3 parameters for a total of 7 
parameters.  Therefore, the only information required from a fit Lifetimes model are these
7 parameters, and value prediction is relatively trivial. These parameters are combined 
with a handful of pieces of information about a customer 
(recency, frequency, time-since-first-order, and average AGP.)  
The sparsity of information is both the elegance of the model but also suggests 
that our expectations for what can be achieved should be limited.  

Parameters were found to be different across IE and ES in initial testing.  These tables are not intended to be maintained, 
but only to give one an idea how different the parameters can be in different Vistaprint markets. 

| Model | Parameter Name | IE | ES |
| :--- | :---: | ---: | ---: |
| MBG | r     | 0.63389 | 1.04064 |
| MBG | alpha | 191.934 | 282.646 |
| MBG | a     | 1.49741 | 1.30952 |
| MBG | b     | 1.43649 | 0.94924 |
| GG  | p     | 1.90000 | 1.44978 |
| GG  | q     | 4.06103 | 4.17221 | 
| GG  | v     | 48.4562 | 54.7713 |
| BG  | r     | 0.15018 | 0.13981 |
| BG  | alpha | 80.3083 | 74.4559 |
| BG  | a     | 1.52276 | 2.69442 |
| BG  | b     | 2.69442 | 2.34253 |

*The MBG-GG model is presently used, alternative model parameters are documented for reference.
These parameters are based on model fit from 2010-07-01 through 2020-08-01.  The 
MBG and GG figures are from v01.4 data, while BG figures are from v0.13 data.*

Parameters interact with one another, so these should not be thought of as independent parameters.  For example:
```
Estimated Population Mean AGP = v * p / (q - 1)
```
So if the dynamic of fitting causes p to be larger, then v will be forced lower so the total value will be held in balance with customer AGP.



#### Calibration and Holdout

Analysis of the calibration and holdout can be performed offline with classes from lifetime-dev.
The raw data set needs to be made available for calibration and holdout, and one challenge is managing
these datasets which (except for a few small samples) are best kept outside the git repo for size reasons.

The data sets need to be copied from the databricks environment to the local drive for this analysis.

Various files exist for analyzing calibration and holdout.
Most relevant:
* daily_v_weekly.py: runs datasets for daily and weekly and compares results

There are some workflows setup around running a "fit_xxx.py" file to compute calibrationa nd holdout scores
and then running a xxx_sensitivity_plots.py to review results.  These are merely example files and not long term code.

The VPLifetimesHoldoutPlots.py class provides methods for comparing calibration and holdout sets.

##### How many years of training data is required?

Whether one is launching a new site to use VPLifetimes or pulling monolith data, we need to understand
how many years of data are required to make accurate predictions.  

To answer this question, we created two holdout sets: one for 2019-2020 (Covid-19 and new site launches) and another
for 2018-2019 to avoid those discontinuities.  In each case, we calibrated with 1-9 years of data and plotted
how the calibration period impacted metrics (RMSE, MAE, and RSpearman).

Under the covers, these longer calibration periods can only have the impact of changing the seven model
parameters.  Therefore, we can also ask how these additional years of training impact parameters.

Findings are included for both IE and ES websites.
The Spearman R increased in value until about six or seven years of training occurred, and then tended to plateau.
MAE and RMSE measures continued to improve materially on the holdout set with additional years of training until the maximum
test length of nine years.

These findings indicate that a long duration training is critical to improving predictions.  Since this long
duration involves pulling data from further back in time, it is more difficult to obtain this data and it also 
suggests that these models will not be able to detect changes which occur in order propensity and value since
longer time periods will not be sensitive to new developments.

Charts for Spain and Ireland:

![es-exp_order_training](docs/source/years_training_exporders_ES.png)

![es-agp_training](docs/source/years_training_agp_ES.png)

![ie-exp_order_training](docs/source/years_training_exporders_IE.png)

![ie-agp_training](docs/source/years_training_agp_IE.png)

Stability plots suggest how parameters the seven parameters are impacted by years of training.
Separate stability plots exist for the MBG and Gamma-Gamma parameters since these sub-models
are trained separately.  These charts are specific to the 2019-2020 holdout for Spain:

![es-training-stability-beta](docs/source/years_training_mbg_stability_2020_ES.png)

![es-training-stability-gamma](docs/source/years_training_gamma_stability_2020_ES.png)

*Reviewing these charts, it appears that significant movements happens with the gamma-gamma model
in years 7-9.  While this may improve accuracy, it is also troubling because one does not want to
reach that far back into the past to learn about spending patterns since pricing strategies change
materially over this timeframe.*

These are the corresponding Ireland charts:

![ie-training-stability-beta](docs/source/years_training_mbg_stability_2020_IE.png)

![ie-training-stability-gamma](docs/source/years_training_gamma_stability_2020_IE.png)

#### Regularization

Lifetimes offers options to regularize two hyperparameters: penalizers for the beta and gamma models.
Based on early experimentation, we may be steering away from using these regularization options.

Reasons to avoid:
* Theoretical.  The regularization coefficients migrate the model away from the statistical model envisioned 
in the literature.
* Interpretative.  The regularization coefficients can easily cause the model to end up in situations which
are far afield from the model design.  For example, the core of the gamma model is a Bayesian model weighing
a prior for expected spend (community mean) with information about an individual.  However, a non-zero regularization 
coefficient can easily make the prior negative, which is non-sensical.
The population mean is expressed:
```
population_mean = v * p / (q - 1)
```
With q < 1, this value goes negative, and in practice we see q values less than one can emerge quite easily.
* Implementation.  The code related to regularization treats every parameter the same without normalizing the parameters
based on their values. Here is the code:
```
penalizer_term = penalizer_coef * sum(params ** 2)
```
This penalization will fall more heavily on parameters which happen to have larger values. 
* Parameter Interaction.  The parameters in the Lifetimes model are not independent.  Instead, increasing one parameter
causes decreases in other parameters when fitting the model.  Therefore, regularization does not have the effect
of avoiding overfitting by reducing model complexity, but instead causes unforeseen interactions among parameters.
This push and pull can be seen in the stability plots we have created.

Here is an example of the interdependence of parameters in the gamma-gamma code:
```
individual_weight = p * frequency / (p * frequency + q - 1)
population_mean = v * p / (q - 1)
```
###### Regularization Sensitivity and Stability

*Note: these sensitivity and stability charts are not up to date for version 0.13 of data.*

**Modified-Beta Coefficients**

Stability is the degree to which the model (parameters) change with different training.  Here are example
chart showing stability with different beta coefficients.  These charts can be produced with
VPLifetimesHoldoutPlots.plot_parameter_stability_pairplot():

![beta_stability](docs/source/beta-stability-ES_-_Beta_Regularization_Sensitivity_Plot.png)<!-- .element height="50%" width="50%" -->

*The PairPlot above illustrates the degree of variable interdependence reflected in regularization.
Dramatic changes of variables can occur as a result.*

Sensitivity is the degree to which the predictions would change based on different parameters.
The function VPLifetimesHoldoutPlots.plot_beta_sensitivity_sets() can show the sensitivity to different
training assumptions.  The following chart shows sensitivity to regularization assumptions for the 
ES 2010-2020 data set:

![beta_senitivity](docs/source/beta-sensitivity-ES_-_Beta_Regularization_Sensitivity_Plot.png)
*The thick line in the chart above is for no regularization, which we recommend for use
in this library.*


**Gamma-Gamma Coefficients**

The Gamma-Gamma stability chart below shows how the penalizer coefficient impacts the 
estimated values for the three model parameters (p, q, v).  Again we do not recommend
using regularization values other than 0, but this chart is useful to understand potential
impacts of this decision.  For example, q<1 makes no conceptual sense but can be seen in these
PairPlots:

![gamma_stability](docs/source/gamma-stability-ES_-_GammaGamma_Regularization_Plot.png)

Sensitivity shows the degree to which the Gamma-Gamma parameter assumptions translate
into predictions.  The solid line on the chart may be helpful in discussing how the customer history is 
translated into future expected spend for the base-case (no regularization).

![gamma_sensitivity](docs/source/gamma-sensitivity-ES_-_GammaGamma_Regularization_Plot.png)<!-- .element height="50%" width="50%" -->
*Above chart shows the preferred penalizer=0 line in solid against alternative values.
The legend has the penalizer value along with the community mean and estimated p, q, and v.  The
unusual negative community mean values which result from q<1 can be seen in this legend.*

#### Daily v. Weekly Granularity

The Lifetimes model can work with daily granularity or weekly granularity.
An initial review suggests that weekly granularity can achieve the same or marginally better
performance than daily granularity.

| Frequency | Country | End Date | AGP MAE | AGP RMSE | AGP RSpearman |
| :--- | :--- | --- | ---: | ---: | ---: |
| D | IE      | 2020-08-01   | 3.33  | 14.63  | 0.239 |
| W | IE      | 2020-08-01   | 3.30  | 14.29  | 0.240 |
| D | ES      | 2020-08-01   | 2.35  | 10.29  | 0.302 |
| W | ES      | 2020-08-01   | 2.32  | 10.02  | 0.303 |

The results seem insignificantly better, but if a wider gap were to occur one would revisit
whether to prefer weekly models.  Operationally, a weekly model might pose some challenges around
fractional weeks for the start of the history and the daily update process.

### Notes and Commentary - Thoughts for Development

* Should we develop cohort metrics, and how would we exclude those which are very inactive from these metrics? 
* We may want to have a “spend bias” adjustment factor saved with each model, so we multiply all the AGP by this bias factor to calibrate to spend seen in the holdout period.
* Who do we include in these models?
  * We can score customers who only shopped one time but we cannot fit with them
* Should we check for accuracy of this model v. CLV in practice?

## Biography

* [Bruce Hardie Gamma-Gamma Model](http://www.brucehardie.com/notes/025/gamma_gamma.pdf)
