import os
import logging
import pandas as pd
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis
pd.options.display.max_columns = 50

#
# - General Processing
#
log_dir = os.path.join('..', '..', 'logs', 'temp')
log_filename = os.path.join(log_dir, 'model-fitting.log')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
if os.path.exists(log_filename):
    os.remove(log_filename)
logging.basicConfig(filename=log_filename,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger('__name__')


#
# Fit data and store descriptor of the case and resulting parameters
#

country = 'IE'
start_date = '2015-07-01'
end_date = '2020-07-01'
src_file = f'VPLifetimes--orders--{country}--{start_date}-{end_date}--{start_date}-{end_date}.0.1.pkl'
src = os.path.join('..', '..', 'data', '01_raw', src_file)
ie_orders_df = pd.read_pickle(src)
model_type = 'ModifiedBetaGeoFit'


country = 'IE'
model_results = []
for observation_date in tqdm(['2016-12-31', '2017-12-31', '2018-12-31', '2019-12-31', '2020-06-30']):
    orders_df = ie_orders_df
    vp_lifetime_analysis = VPLifetimesAnalysis(VPLifetimesData(ie_orders_df))
    results, _, _ = vp_lifetime_analysis.fit_order_data(end_date)
    results['country'] = country
    results['cohort'] = ''
    results['start_date'] = start_date
    results['end_date'] = observation_date
    results['seasonal'] = ''
    results['sample_percent'] = 100
    model_results.append(results)


df = pd.DataFrame.from_dict(model_results)

# one time population of data set
# df.to_csv(os.path.join('..', '..', 'data', '09_test_data', 'test_IE_2020-07-17-1.csv'))
