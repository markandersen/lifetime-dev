import os
import logging
import pandas as pd
import numpy as np
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis
pd.options.display.max_columns = 50

#
# - General Processing
#
log_dir = os.path.join('..', '..', 'logs', 'temp')
log_filename = os.path.join(log_dir, 'model-fitting.log')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
if os.path.exists(log_filename):
    os.remove(log_filename)
logging.basicConfig(filename=log_filename,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger('__name__')

#
# Fit data and store descriptor of the case and resulting parameters
#

country = 'ES'
start_date = '2015-07-01'
end_date = '2020-07-01'
src_file = f'VPLifetimes--orders--{country}--{start_date}-{end_date}--{start_date}-{end_date}.0.1.pkl'
src = os.path.join('..', '..', 'data', '01_raw', src_file)
orders_df = pd.read_pickle(src)
model_type = 'ModifiedBetaGeoFit'

shoppers = orders_df.SHOPPER_ID.unique()
shoppers_df = pd.DataFrame(shoppers, columns=['SHOPPER_ID'])
print(f'Starting with {len(shoppers_df)} shoppers and {len(orders_df)} orders')

model_results = []
observation_date = '2020-06-30'
num_tests = 10
np.random.seed(seed=420)
for i in tqdm(range(num_tests)):
    shoppers_filter = shoppers_df.sample(frac=0.7)
    orders_subset = orders_df.merge(shoppers_filter, how='inner')
    print(f'Shoppers count {len(shoppers_filter)} and orders count {len(orders_subset)}')
    vp_lifetime_analysis = VPLifetimesAnalysis(VPLifetimesData(orders_subset))
    results, _, _ = vp_lifetime_analysis.fit_order_data(observation_date)
    results['country'] = country
    results['cohort'] = f'random-{i}'
    results['start_date'] = start_date
    results['end_date'] = observation_date
    results['seasonal'] = ''
    results['sample_percent'] = 0.7
    model_results.append(results)

df = pd.DataFrame.from_dict(model_results)

# one time population of data set
df.to_csv(os.path.join('..', '..', 'data', '09_test_data', 'test_ES-2020-07-17-1.csv'))
assert len(df) == num_tests, f'Incorrect number of tests {len(df)}'
