import numpy as np
import pandas as pd
from src.lifetime_dev.VPLifetimesHoldoutMetrics import VPLifetimesHoldoutMetrics


# tests

ground_truth_columns = ['id', 'frequency_holdout', 'monetary_value_holdout']
prediction_columns = ['id', 'predict_order_count', 'predict_spend_per_order', 'predict_total_spend']
gt1 = [
    ['A', 1, 25.0],
    ['B', 2, 50.0]
]
gt1_df = pd.DataFrame(gt1, columns=ground_truth_columns)
pred1 = [
    ['A', 2, 30.0, 60.0],
    ['B', 0, 20.0, 0.0]
]
pred1_df = pd.DataFrame(pred1, columns=prediction_columns)
expect1 = {
    'numorder_mae': 1.5,
    'numorder_mse': 2.5,
    'numorder_rmse': np.sqrt(2.50),
    'numorder_predicted_orders': 2,
    'numorder_actual_orders': 3,
    'numorder_predicted_per_customer': 1.0,
    'numorder_actual_per_customer': 1.5,
    'numorder_rspearman': -1.0,
    'mval_mae': 42.5,
    'mval_mse': (35**2 + 50**2) / 2.0,
    'mval_rmse': np.sqrt((35**2 + 50**2) / 2.0),
    'mval_predicted': 60.0,
    'mval_actual': 75.0,
    'mval_predicted_per_customer': 30.0,
    'mval_actual_per_customer': 37.5,
    'mval_rspearman': -1.0
}

gt2 = [
    ['A', 1, 25.0],
    ['B', 2, 50.0]
]
gt2_df = pd.DataFrame(gt2, columns=ground_truth_columns)
pred2 = [
    ['A', 1, 25.0, 25.0],
    ['B', 2, 25.0, 50.0]
]
pred2_df = pd.DataFrame(pred2, columns=prediction_columns)
expect2 = {
    'numorder_mae': 0.0,
    'numorder_mse': 0.0,
    'numorder_rmse': np.sqrt(0.0),
    'numorder_predicted_orders': 3,
    'numorder_actual_orders': 3,
    'numorder_predicted_per_customer': 1.5,
    'numorder_actual_per_customer': 1.5,
    'numorder_rspearman': 1.0,
    'mval_mae': 0.0,
    'mval_mse': 0.0,
    'mval_rmse': 0.0,
    'mval_predicted': 75.0,
    'mval_actual': 75.0,
    'mval_predicted_per_customer': 37.5,
    'mval_actual_per_customer': 37.5,
    'mval_rspearman': 1.0
}


def internal_test_helper(test_label, gt_df, pred_df, expectation):
    """
    tests a ground truth and prediction to generate expected dictionary of results
    """
    compared = VPLifetimesHoldoutMetrics(gt_df, pred_df)
    for key in expectation:
        if compared.base_metrics[key] != expectation[key]:
            print(f'Test {test_label} metric {key} was {compared.base_metrics[key]} but expected {expectation[key]}.')
        assert compared.base_metrics[key] == expectation[key], f'Mismatch on key {key} in test {test_label}'
    return compared


def test_function():
    """
    Run pytests
    """
    _ = internal_test_helper('1', gt1_df, pred1_df, expect1)
    _ = internal_test_helper('2', gt2_df, pred2_df, expect2)
