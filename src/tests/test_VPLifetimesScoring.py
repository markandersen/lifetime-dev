import os
from tqdm import tqdm
import pandas as pd
from lifetimes.utils import summary_data_from_transaction_data
from lifetimes import GammaGammaFitter, ParetoNBDFitter, ModifiedBetaGeoFitter, BetaGeoBetaBinomFitter
from src.lifetime_dev.VPLifetimesScoring import VPLifetimesScoring
pd.options.display.max_columns = 40


def get_test_data(end_date, min_date):
    """
    gets test data for this script
    """
    countries = 'IE-ES'
    start_date = '2010-07-01'
    src_file = f'VPLifetimes--orders--{countries}--{start_date}-{end_date}--{start_date}-{end_date}.0.11.pkl'
    # removed two '..' from path so this code runs from top level dir
    src = os.path.join('data', '09_test_data', src_file)
    orders_df = pd.read_pickle(src)
    ie_orders_df = orders_df[orders_df.ACCOUNT_CREATION_COUNTRY == 'IE'].copy()
    ie_orders_df['order_date_dt'] = pd.to_datetime(ie_orders_df.ORDER_DATE, format='%Y-%m-%d')
    subset = ie_orders_df[ie_orders_df.order_date_dt > pd.to_datetime(min_date, format='%Y-%m-%d')]
    subset.AGP_BUDGET_USD = subset.AGP_BUDGET_USD.astype('float64')  # ensure float64
    return subset


def test_vp_lifetimes_scoring():
    #
    # Test VPLifetimesScoring - class which will be used by Databricks to score customers
    #
    # Compare VPLifetimesScoring with calling the predict functions directly after fit
    #
    end_date = '2020-07-21'
    min_date = '2019-07-01'
    test_data = get_test_data(end_date=end_date, min_date=min_date)

    # forecast days does not work with the Lifetimes CLV model which works in months
    # it is very slow.  But we will use it for comparison here
    forecast_months = [1, 3, 6, 12]
    forecast_days = [30 * i for i in forecast_months]
    print(f'Using subset of orders starting {min_date}: {len(test_data)}')

    summary_df = summary_data_from_transaction_data(
        test_data,
        'SHOPPER_ID',
        'ORDER_DATE',
        'AGP_BUDGET_USD',
        '%Y-%m-%d',
        end_date,
        freq='D',
        freq_multiplier=1,
        include_first_transaction=False)
    print(f'{len(summary_df)} summary shoppers created by summary_data_from_transaction_data')

    #
    # Manual fitting and predicting with Lifetimes
    #
    # Fit
    #
    mgbf = ModifiedBetaGeoFitter(penalizer_coef=0.0)
    mgbf.fit(summary_df.frequency,
             summary_df.recency,
             summary_df['T'])
    print('Lifetimes library ModifiedBetaGeoFit results:')
    print(mgbf.summary)

    ggm = GammaGammaFitter(penalizer_coef=0.0)
    subset_ggm_fit = summary_df[summary_df.frequency > 0]
    print(f'{len(subset_ggm_fit)} customers with frequency greater than 0 for fitting purposes')
    ggm.fit(subset_ggm_fit.frequency, subset_ggm_fit.monetary_value)
    print('Lifetimes library GammaGammaFitter results:')
    print(ggm.summary)

    # Predict
    #
    summary_df['prob_alive'] = mgbf.conditional_probability_alive(summary_df.frequency,
                                                                  summary_df.monetary_value,
                                                                  summary_df['T'])

    summary_df['conditional_expected_value_per_order'] = \
        ggm.conditional_expected_average_profit(
            summary_df.frequency,
            summary_df.monetary_value
        )
    for forecast_month in tqdm(forecast_months):
        forecast_duration = forecast_month * 30
        summary_df[f'fcst_orders_{forecast_duration}'] = \
            mgbf.conditional_expected_number_of_purchases_up_to_time(
                forecast_duration,  # later optimization maybe can do in one pass
                summary_df.frequency,
                summary_df.recency,
                summary_df['T']
            )
        summary_df[f'fcst_value_{forecast_duration}'] = \
            ggm.customer_lifetime_value(
                mgbf,
                summary_df.frequency,
                summary_df.recency,
                summary_df['T'],
                summary_df.monetary_value,
                time=forecast_month,  # hard coded to months and must be type int to work and match
                # filed an Issue with Lifetimes about fact that it is documented as a float
                # but only works for int months
                discount_rate=0.0,
                freq='D'
            )
    summary_df = summary_df.reset_index()
    print(f'Completed manual scoring for customers: summary_df length {len(summary_df)}')

    # ------------------------------------------------------------------------
    #
    # Test VPLifetimesScoring to ensure it has same results
    #
    # use parameters from fitting above
    repeat_params = mgbf.params_
    gg_params = ggm.params_

    print('Testing VPLifetimesScoring.')
    print('Summarizing data')
    scorer = VPLifetimesScoring(test_data,
                                'ModifiedBetaGeo',
                                repeat_params,
                                gg_params,
                                end_date,
                                'SHOPPER_ID',
                                'AGP_BUDGET_USD',
                                'ORDER_DATE',
                                forecast_days=forecast_days.copy())
    print(f'VPLifetimesScoring summarizes and has {len(scorer.summary_df)} summary records')
    print('Scoring customers')
    sum_result = scorer.score_customers()
    print(f'Completed automatic scoring of customers with length {len(sum_result)} customers in sum_result')
    # round columns in two DataFrames to make comparisons reasonable
    num_digits = 4
    for duration in forecast_days:
        for prefix in ['fcst_value', 'fcst_orders']:
            field_name = f'{prefix}_{duration}'
            sum_result[field_name] = sum_result[field_name].round(num_digits)
            summary_df[field_name] = summary_df[field_name].round(num_digits)

    # Go through columns one at a time so we can identify where issue is:
    for col in sum_result.columns:
        print(f'Checking column {col}')
        assert sum_result[col].equals(summary_df[col]), f'Mismatch on {col}'
    assert sum_result.equals(summary_df), 'VPLifetimesScoring does not match manual fit results.'
