import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import logging
import lifetimes
from lifetimes import GammaGammaFitter, ParetoNBDFitter, ModifiedBetaGeoFitter, BetaGeoBetaBinomFitter, BetaGeoFitter
from lifetimes.plotting import plot_probability_alive_matrix
from lifetimes.plotting import plot_frequency_recency_matrix
from src.lifetime_dev.VPLifetimesData import VPLifetimesData

print(f'lifetimes version: {lifetimes.__version__}')

class VPLifetimesAnalysis:

    def __init__(self, lifetimes_data: VPLifetimesData):
        """
        Fitters for Lifetimes data coupled to a specific lifetimes data object
        and assumptions about how that object will be used

        :param lifetimes_data: Lifetimes data object
          this object has the data and field names
        """
        self.lifetimes_data = lifetimes_data
        # remember order model and its characteristics for potential recording
        self.order_model = None
        self.order_penalizer = 0.0
        self.model_type = None
        # gamma-gamma model is specific
        self.gg_model = None
        self.gg_penalizer = 0.0

    @staticmethod
    def get_model_types():
        """
        Return: Possible model types
        """
        return ['BetaGeo', 'ModifiedBetaGeo', 'BetaGeoBetaBinom', 'ParetoNBD']

    def reset_order_model(self,
                          model_type: str = 'ModifiedBetaGeo',
                          penalizer: float = 0.01):
        """
        reset the fitting model for this object and clear resulting variables

        :param model_type: model type to sue
        :param penalizer: penalizer to pass into the model
        :return: fit model
        """
        assert model_type in VPLifetimesAnalysis.get_model_types(), f'model type {model_type} not recognized'
        self.model_type = model_type
        self.order_penalizer = penalizer
        if self.model_type == 'BetaGeo':
            # BG/NBD model
            self.order_model = BetaGeoFitter(penalizer_coef=self.order_penalizer)
        elif self.model_type == 'ModifiedBetaGeo':
            self.order_model = ModifiedBetaGeoFitter(penalizer_coef=self.order_penalizer)
        elif self.model_type == 'BetaGeoBetaBinom':
            # Beta-Geometric/Beta-Binomial
            self.order_model = BetaGeoBetaBinomFitter(penalizer_coef=self.order_penalizer)
        elif self.model_type == 'ParetoNBD':
            # ParetoNBD
            self.order_model = ParetoNBDFitter(penalizer_coef=self.order_penalizer)
        else:
            assert False, 'inconsistent model type - programming error'
        print(f'Model type set to {self.model_type}.  Fit results cleared.')
        return self.order_model

    def fit_order_data(self,
                       observation_date,
                       prediction_model_penalizer=0.0,
                       gg_penalizer=0.0,
                       model_type: str = 'ModifiedBetaGeo',
                       freq: str = 'D',
                       freq_multiplier: int = 1):
        """
        Fits order data with prediction and monetary spend models returns the parameters resulting

        :param observation_date: observation date to limit observations in passed data set
        :param prediction_model_penalizer: penalizer for prediction model
        :param gg_penalizer: penalizer for gamma-gamma model
        :param model_type: type of model which will be fit
        :param freq: frequency freq to pass to lifetimes ('D', 'M')
        :param freq_multiplier: pass to lifetimes
        :returns: resulting parameters both in a single dictionary with distinguishing names and
          as separate dictionaries (<single_dict>, <predict_dict>, <gamma-gamma-dict>)
        """
        summary_data = self.lifetimes_data.get_summary_data_from_transaction(observation_date,
                                                                             freq=freq,
                                                                             freq_multiplier=freq_multiplier)
        return self.fit(summary_data, 'frequency', 'recency', 'T', 'monetary_value',
                        prediction_model_penalizer, gg_penalizer, model_type)

    def fit(self,
            summary_data,
            frequency_field,
            recency_field,
            t_field,
            monetary_value_field,
            prediction_model_penalizer=0.0,
            gg_penalizer=0.0,
            model_type: str = 'ModifiedBetaGeo'):

        """
        Fits order data with prediction and monetary spend models returns the parameters resulting

        :param summary_data: data to fit
        :param frequency_field: field name
        :param recency_field: field name
        :param t_field: field_name
        :param monetary_value_filed: field name
        :param prediction_model_penalizer: penalizer for prediction model
        :param gg_penalizer: penalizer for gamma-gamma model
        :param model_type: type of model which will be fit

        :returns: resulting parameters both in a single dictionary with distinguishing names and
          as separate dictionaries (<single_dict>, <predict_dict>, <gamma-gamma-dict>)
        """
        self.reset_order_model(model_type=model_type,
                               penalizer=prediction_model_penalizer)
        self.order_model.fit(summary_data[frequency_field],
                             summary_data[recency_field],
                             summary_data[t_field])
        posfreq_summary_data = summary_data[summary_data[frequency_field] > 0]
        self.gamma_gamma_fit(posfreq_summary_data[frequency_field],
                             posfreq_summary_data[monetary_value_field],
                             penalizer=gg_penalizer)
        #
        # parameters from models as shorthand
        #
        fit_parameters = {}
        for param in self.order_model.params_.keys():
            fit_parameters[f'b-{param}'] = self.order_model.params_[param]
        for param in self.gg_model.params_.keys():
            fit_parameters[f'g-{param}'] = self.gg_model.params_[param]
        return fit_parameters, self.order_model.params_, self.gg_model.params_

    def gamma_gamma_fit(self,
                        frequency_series: pd.Series,
                        monetary_series: pd.Series,
                        penalizer: float = 0.0):
        """
        Fits gamma-gamma model: either to whole set or a calibration set

        :param frequency_series: pandas series with the frequency data
        :param monetary_series: pandas series with the monetary serires
        :param penalizer: penalizer for the gamma-gamma model
        :return: fit gamma-gamma model; side effect saved in instance
        """
        log = logging.getLogger(__name__)
        temp = frequency_series[frequency_series == 0]
        if len(temp) > 0:
            log.info(f'Gamma-Gamma cannot be fit against zero frequency, {len(temp)} records are excluded')
        positive_frequency_observations = frequency_series[frequency_series > 0]

        self.gg_model = GammaGammaFitter(penalizer_coef=penalizer)
        self.gg_model.fit(positive_frequency_observations, monetary_series)
        log.info(self.gg_model)
        return self.gg_model

    def calculate_plot_expected_purchases(self, up_to_time_list=[30, 90, 365], max_x=3.0, verbose=False):
        """
        calculates and then plots expected number of purchases for set of shoppers

        up_to_time_list: the list of times to calculate and then plot
        max_x: maximum number of purchases shown on histogram
        verbose: used if beta_fit is triggered by this function
        """
        for period in up_to_time_list:
            self.calculate_conditional_expected_number_of_purchases(period, verbose)

        fig, allax = plt.subplots(3, sharex=True, sharey=False, figsize=(12,8))
        for cntr, period in enumerate(up_to_time_list):
            print(f'plotting ax {cntr}')
            ax = allax[cntr]
            chart_pdf = self.pdf[self.pdf[f'cond_purchases_{period}']<max_x]
            chart_pdf[f'cond_purchases_{period}'].plot.hist(bins=30, ax=ax)
        ax.set_xlim(0, max_x)
        plt.suptitle(f"Expected number of purchases for {up_to_time_list} days")
        plt.tight_layout()
        plt.subplots_adjust(top=0.85)
        plt.show()

    #
    # - Plotting: TBD move out
    #
    def plot_frequency_recency_matrix(self):
        """
        plots frequency and recency matrix
        """
        # TODO: self.bgf no longer proxy for having been fit
        print('Deprecated')
        assert self.bgf is not None, 'bgf must be fit before calling plot_frequency_recency_matrix'
        plot_frequency_recency_matrix(self.bgf)
        plt.show()

    def plot_conditional_probability_alive(self, plot_hist=True, plot_split_hist=False, plot_matrix=True):
        """
        reports and plots information about conditional probability alive

        plot_hist: boolean whether to plot histogram
        plot_split_hist: plot separate histogram for single and multi-purchasers
                      useful for understanding how MBG v. BG work
        plot_matrix: boolean whether to plot matrix
        """
        print('Deprecated')
        assert self.bgf is not None, 'bgf fitting must occur before plotting conditional probability alive'
        assert 'cond_prob_alive' in self.pdf.columns, 'cond_prob_alive must be calculated before plotting conditional probability alive'
        prob_alive_1 = len(self.pdf[self.pdf.cond_prob_alive==1])
        prob_alive_not_1 = len(self.pdf) - prob_alive_1
        print(f'Num pegged at prob_alive=1 {prob_alive_1} out of {len(self.pdf)} records or {100*(np.round(prob_alive_1/len(self.pdf),3))}%')
        palive_customers = self.pdf[self.pdf.cond_prob_alive<1]
        if plot_hist:
            ax = palive_customers.cond_prob_alive.hist(bins=np.arange(0.0, 1.0, 0.05), figsize=(14,5))
            ax.set_title(f'Conditional Probability Alive (shoppers with valid estimates < 1.0): {prob_alive_not_1} out of {len(self.pdf)} records', fontsize=18)
            plt.show()

        if plot_split_hist:
            fig, axes = plt.subplots(2, sharex=True, sharey=False, figsize=(14,5))
            self.pdf[self.pdf[self.fcol]>0].cond_prob_alive.hist(bins=np.arange(0.0, 1.0, 0.05), ax=axes[0])
            self.pdf[self.pdf[self.fcol]==0].cond_prob_alive.hist(bins=np.arange(0.0, 1.0, 0.05), ax=axes[1])
            plt.suptitle(f'Conditional Probability Alive Comparing Multi-Shoppers (top) and Single-Shoppers (bottom)', fontsize=18)
            plt.tight_layout()
            plt.subplots_adjust(top=0.85)
            plt.show()

        if plot_matrix:
            plot_probability_alive_matrix(self.bgf)
            plt.show()
