import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.lines import Line2D
import numpy as np
import pandas as pd
import seaborn as sns
from functools import partial
from lifetimes.plotting import plot_calibration_purchases_vs_holdout_purchases, plot_period_transactions
from src.lifetime_dev.VPLifetimesHoldoutMetrics import VPLifetimesHoldoutMetrics


class VPLifetimesHoldoutPlots:
    """
    VPLifetimesHoldoutPlots

    Plots and Metrics For Evaluating Calibration and Holdout for Lifetimes

    The calibration-holdout dataframe (standard Lifetimes format) is fixed at initialization.

    Different fit models can be examined against that set for their ability to fit.
    Therefore the fit models are not used in the constructor but provided through methods.
    """

    def __init__(self,
                 calibration_holdout_df: pd.DataFrame):
        """

        :param calibration_holdout_df: Lifetimes standard calibration-holdout DataFrame
        """
        self.calibration_holdout_df = calibration_holdout_df

    def plot_default_lifetimes_charts_prob_alive(self,
                                                 fit_model,
                                                 dataset_descriptor: str,
                                                 show: bool = True,
                                                 figsize: (int, int) = (10, 10)):
        """
        Displays the default lifetimes charts for probability alive
        on a single chart with two subplots and a title.

        :param fit_model: the model which has been fit
        :param dataset_descriptor: portion of title for chart
        :param show: whether to invoke plt.show()
        :param figsize: figure size for the combined chart
        :return: None
        """
        fig, axes = plt.subplots(2, figsize=figsize)
        VPLifetimesHoldoutPlots._plot_calibration(fit_model,
                                                  self.calibration_holdout_df,
                                                  axes[0])
        VPLifetimesHoldoutPlots._plot_period_transactions(fit_model, axes[1])
        plt.suptitle(f'Lifetime Charts for Holdout: {dataset_descriptor}')
        fig.tight_layout()
        plt.subplots_adjust(0.8)
        if show:
            plt.show()

    @staticmethod
    def _plot_calibration(fit_model, calibration_holdout_df, ax=None):
        """
        standard plot_calibration_vs_holdout_puchases

        fit_model: model which is being used for calculations
        calibration_holdout_df: the dataframe holding the data
        ax: axes on which to plot the chart
        """
        # bgf should be fit before this -- should add assertion TBD - see self.bgf or self.calholddata
        assert fit_model is not None, "Must fit model before _plot_calibration"
        ax = plot_calibration_purchases_vs_holdout_purchases(fit_model,
                                                             calibration_holdout_df,
                                                             kind='frequency_cal',
                                                             ax=ax)
        return ax

    @staticmethod
    def _plot_period_transactions(fit_model, ax=None):
        """
        Standard Lifetimes plot_period_transactions on self.fit_model.

        ax: axes on which to plot the chart
        """
        assert fit_model is not None, "Must fit model before _plot_period_transactions"
        ax = plot_period_transactions(fit_model, ax=ax)
        return ax

    def plot_custom_holdout_evaluations_prob_alive(self,
                                                   dataset_descriptor: str,
                                                   figsize: (int, int) = (14, 18)):
        """
        plots two custom charts for model evaluation
        these methods could be called separately if only one is desired

        :param dataset_descriptor: descriptor which will be part of title
        :param figsize: size of the two figures

        :return: None
        """
        print('Warning: Deprecation.  No file save will take place if this method is called.')
        self.plot_holdout_frequency_charts(suptitle=f'Frequency holdouts: {dataset_descriptor}',
                                           figsize=figsize)
        self.plot_model_rank_vs_holdout(suptitle=f'Rank v. holdout: {dataset_descriptor}',
                                        figsize=figsize)

    def plot_holdout_frequency_charts(self,
                                      suptitle: str,
                                      viz_max_orders: int = 6,
                                      viz_max_frequency_holdout: int = 6,
                                      figsize: (int, int) = (14, 18),
                                      color_below_median: str = 'red',
                                      linestyle_mean_bar: str = '--',
                                      add_median_mean_text: bool = True):
        """
        plot holdout_frequency v. predicted frequency for orders

        :param suptitle: title to use for the plot
        :param viz_max_orders:
        :param viz_max_frequency_holdout:
        :param figsize:
        :param color_below_median:
        :param linestyle_mean_bar:
        :param add_median_mean_text:
        :return:
        """
        calhold = self.calibration_holdout_df
        subplot_count = viz_max_frequency_holdout
        fig, axes = plt.subplots(subplot_count,
                                 sharex=True,
                                 figsize=figsize)
        plt.subplots_adjust(top=0.95)
        #
        for frequency in range(viz_max_frequency_holdout):
            #
            # For each frequency in actual holdout period, show what the model had estimated
            # and kde for the model estimation histogram
            #
            ax = axes[frequency]
            freq_holdout_match_df = calhold[calhold.frequency_holdout == frequency]
            mean_predict = np.round(freq_holdout_match_df.model_predictions.mean(), 2)
            median_predict = np.round(freq_holdout_match_df.model_predictions.median(), 2)
            # mode depends on precision and the histogram bins are adequate for this.
            #
            # would like bins to be fixed over the visible range
            # but the kde needs the entire range even that which is not shown
            # and if we draw these separately then the ylimit might not match so I will
            # live with the variation in histogram bin size for now.
            #
            sns.distplot(freq_holdout_match_df.model_predictions, bins=viz_max_orders * 40, ax=ax)
            ax.set_title(f'KDE plot for holdout = {frequency}')
            ymin, ymax = ax.get_ylim()
            ax.set_xlim(0, viz_max_orders)
            ax.set_xticks(np.arange(0, viz_max_orders, 0.5))

            # color bars below median: default to color these but can be skipped if None is passed
            if color_below_median is not None:
                for patch in ax.patches:
                    left, bottom = patch.get_xy()
                    if left < median_predict:
                        patch.set_color(color_below_median)

            # add vertical line for median / mean
            if linestyle_mean_bar is not None:
                ax.axvline(x=mean_predict, linestyle=linestyle_mean_bar, color='xkcd:gray')
            if add_median_mean_text:
                ax.text(mean_predict, ymax * 0.9, f'Mean: {mean_predict}, Median: {median_predict}')
            # suppress xlabels on intermediate charts
            if frequency != subplot_count - 1:
                ax.set_xlabel('')
        plt.suptitle(suptitle, y=.98)
        plt.tight_layout(rect=(0, 0, 1, 0.96))

        #
        # - Chart ranking of customer on order-propensity: prediction v. actual
        #
        # Recommend using this with jittering on the y axis
        #

    def plot_model_rank_vs_holdout(self,
                                   suptitle: str,
                                   viz_max_orders: int = 6,
                                   figsize: (int, int) = (14, 32),
                                   jitter_freq_holdout_y: bool = True,
                                   num_buckets: int = 20):
        """
        plots scatter of model ranking v. performance in holdout period

        :param suptitle: title to use for the chart
        :param viz_max_orders:
        :param figsize: size of final figure
        :param jitter_freq_holdout_y: bool - whether to jitter the final chart
        :param num_buckets: number of buckets to use for the histogram
        :return:
        """

        calhold = self.calibration_holdout_df
        subplot_count = 6
        # make subplots if we are doing a visualization
        fig, axes = plt.subplots(subplot_count, sharex=False, figsize=figsize)
        plt.subplots_adjust(top=0.95)

        rho_df = calhold[['frequency_holdout', 'model_predictions']].corr(method='spearman')
        rho = np.round(rho_df.loc['model_predictions', 'frequency_holdout'], 3)
        calhold['model_rank'] = calhold.model_predictions.rank(method='first')
        calhold.sort_values('model_rank', inplace=True)
        if jitter_freq_holdout_y:
            calhold['frequency_holdout_jitter'] = calhold.frequency_holdout + \
                                                  np.random.normal(scale=0.1, size=len(calhold))
            yvar = 'frequency_holdout_jitter'
        else:
            yvar = 'frequency_holdout'
        ax = axes[0]
        calhold.plot.scatter(x='model_rank', y=yvar, s=.1, ax=ax)
        calhold.plot.line(x='model_rank', y='model_predictions', color='xkcd:black', linewidth=1.5, ax=ax)
        # can show predictions
        ax.set_ylim(-0.5, viz_max_orders + 0.5)
        ax.set_yticks(np.arange(0, viz_max_orders + 1))
        ax.set_ylabel('# Purchased (jit)', fontsize=10)
        ax.set_xlabel('Predicted Customer Rank (best=highest number)', fontsize=12)
        ax.annotate(f'Model P(order) Ranking v. Actual Orders\nSpearman R {rho}', xy=(0, 2))

        #
        # Bucket the model_ranks and report on model v. actual by bucket
        #
        ax = axes[1]
        calhold['model_rank_bucket'] = pd.qcut(calhold.model_rank, q=num_buckets, labels=False)
        temp = calhold.groupby(['model_rank_bucket']).agg({'model_predictions': ['min', 'max', 'mean'],
                                                           'frequency_holdout': 'mean'}).reset_index()
        temp.columns = [' '.join(col).strip() for col in temp.columns.values]  # combine multiindex values
        temp.rename(columns={'frequency_holdout mean': 'Mean Actual',
                             'model_predictions mean': 'Mean Prediction'}, inplace=True)
        temp.plot.line(x='model_rank_bucket', y='Mean Actual', color='xkcd:blue', marker='o', markersize=4, ax=ax)
        temp.plot.line(x='model_rank_bucket', y='Mean Prediction', color='xkcd:dark gray', marker='o', markersize=4,
                       ax=ax)
        # dont let optimistic model predictions drive y axis
        ymax = int(temp['Mean Actual'].max()) + 0.5
        ax.set_ylim(0.0, ymax)
        # print(f'ylim max at {int(temp["Mean Actual"].max()) + 0.5}')
        ax.set_ylabel('Mean # Purchases (jit) - per Bucket', fontsize=10)
        ax.set_xlabel(f'Predicted Customer Rank in {num_buckets} Buckets (best=highest number)', fontsize=12)
        ax.set_xticks(np.arange(0, 20))
        ax.annotate('Model P(order) Ranking v. Actual % Orders (mean per bucket)', xy=(0, ymax / 2.0))

        #
        # Cumulative Orders
        #
        ax = axes[2]
        calhold.sort_values('model_rank', ascending=False, inplace=True)
        calhold['Predicted Orders Cum Sum'] = calhold.model_predictions.cumsum()
        calhold['Actual Orders Cum Sum'] = calhold.frequency_holdout.cumsum()
        calhold.plot.line(x='model_rank', y='Predicted Orders Cum Sum', ax=ax)
        calhold.plot.line(x='model_rank', y='Actual Orders Cum Sum', ax=ax)
        # gap calculation and illustration
        max_rank = calhold.model_rank.min()
        max_obs = calhold[calhold.model_rank == max_rank]
        predict_y = max_obs['Predicted Orders Cum Sum'].min()
        actual_y = max_obs['Actual Orders Cum Sum'].min()
        if actual_y == 0:
            y_miss = 'NaN'
        else:
            y_miss = int(100 * (predict_y / actual_y - 1.0))
        l2d = Line2D(xdata=[0, 0], ydata=[predict_y, actual_y], linestyle='dotted', linewidth=1.0, color='xkcd:red')
        ax.add_line(l2d)
        ax.annotate(f'{y_miss}%\nerror', xy=(-5, min(predict_y, actual_y)))

        _, ymax = ax.get_ylim()
        xmin, xmax = ax.get_xlim()
        ax.get_yaxis().set_major_formatter(mtick.FuncFormatter(lambda x, p: format(int(x), ',')))
        ax.set_xlim(xmax, xmin)  # reverse axis
        ax.set_ylabel('Cumulative Orders', fontsize=10)
        ax.set_xlabel('Model Ranked Customers', fontsize=12)
        ax.annotate(f'Cumulative Model Orders v. Actual Orders', xy=(xmax, 0.65*ymax))

        # Monetary Value
        #
        # - Chart ranking of customer on expected profit: prediction v. actual
        # Recommend with jittering on y-axis (still a lot of tie values at 0)
        #
        ax = axes[3]
        rho_df = calhold[['monetary_value_holdout', 'model_prediction_bookings']].corr(method='spearman')
        rho = np.round(rho_df.loc['model_prediction_bookings', 'monetary_value_holdout'], 3)
        calhold['model_rank_money'] = calhold.model_prediction_bookings.rank(method='first')
        calhold.sort_values('model_rank_money', inplace=True)
        if jitter_freq_holdout_y:
            calhold['monetary_holdout_jitter'] = calhold.monetary_value_holdout + np.random.normal(scale=1.0,
                                                                                                   size=len(calhold))
            yvar = 'monetary_holdout_jitter'
        else:
            yvar = 'monetary_value_holdout'
        calhold.plot.scatter(x='model_rank_money', y=yvar, s=.1, ax=ax)
        calhold.plot.line(x='model_rank_money',
                          y='model_prediction_bookings',
                          color='xkcd:black',
                          linewidth=1.5,
                          ax=ax)
        ax.set_ylim(-5, 200.5)
        ax.set_yticks(np.arange(0, 200, 10))
        ax.set_ylabel('Gross Profit(jit)', fontsize=10)
        ax.set_xlabel('Predicted Customer Rank (best=highest number)', fontsize=12)
        ax.annotate(f'Model Gross Profit Ranking v. Actual Gross Profit\nSpearman R: {rho}', xy=(0, 100))

        #
        # Buckets: Bookings v. Actual
        #
        ax = axes[4]
        calhold['model_rank_money_bucket'] = pd.qcut(calhold.model_rank_money, q=num_buckets, labels=False)
        temp = calhold.groupby(['model_rank_money_bucket']).agg({'model_prediction_bookings': ['min', 'max', 'mean'],
                                                                 'monetary_value_holdout': 'mean'}).reset_index()
        temp.columns = [' '.join(col).strip() for col in temp.columns.values]  # combine multiindex values
        temp.rename(columns={'monetary_value_holdout mean': 'Mean Actual',
                             'model_prediction_bookings mean': 'Mean Prediction'}, inplace=True)
        temp.plot.line(x='model_rank_money_bucket',
                       y='Mean Actual',
                       color='xkcd:blue',
                       marker='o',
                       markersize=4,
                       ax=ax)
        temp.plot.line(x='model_rank_money_bucket',
                       y='Mean Prediction',
                       color='xkcd:dark gray',
                       marker='o',
                       markersize=4,
                       ax=ax)
        # dont let optimistic model predictions drive y axis
        ax.set_ylim(0.0, 50.0)
        ax.annotate('Model Gross Profit Ranking v. Actual Gross Profit Performance (mean per bucket)', xy=(0, 30))
        ax.set_ylabel('Mean Gross Profit per Bucket', fontsize=10)
        ax.set_xlabel(f'Predicted Customer Rank in {num_buckets} Buckets (best=highest number)', fontsize=12)

        #
        # Cumulative Bookings
        #
        ax = axes[5]
        calhold.sort_values('model_rank_money', ascending=False, inplace=True)
        calhold['Predicted Profit Cum Sum'] = calhold.model_prediction_bookings.cumsum()
        calhold['Actual Profit Cum Sum'] = calhold.monetary_value_holdout.cumsum()
        calhold.plot.line(x='model_rank_money', y='Predicted Profit Cum Sum', ax=ax)
        calhold.plot.line(x='model_rank_money', y='Actual Profit Cum Sum', ax=ax)
        _, ymax = ax.get_ylim()
        xmin, xmax = ax.get_xlim()
        ax.set_xlim(xmax, xmin)  # reverse axis
        ax.set_ylabel('Cumulative Sum Spend', fontsize=10)
        ax.set_xlabel('Model Ranked Customers', fontsize=12)
        ax.annotate(f'Cumulative Model GP v. Actual GP', xy=(xmax, 0.7*ymax))
        # gap calculation and illustration
        max_obs = calhold[calhold.model_rank == max_rank]  # refresh due to new field added
        predict_y = max_obs['Predicted Profit Cum Sum'].min()
        actual_y = max_obs['Actual Profit Cum Sum'].min()
        if actual_y == 0:
            y_miss = 'NaN'
        else:
            y_miss = int(100 * (predict_y / actual_y - 1.0))
        l2d = Line2D(xdata=[0, 0], ydata=[predict_y, actual_y], linestyle='dotted', linewidth=1.0, color='xkcd:red')
        ax.add_line(l2d)
        ax.annotate(f'{y_miss}%\nerror', xy=(-5, min(predict_y, actual_y)))

        # Full chart decoration and saving
        plt.suptitle(suptitle, y=0.98)
        plt.tight_layout(rect=(0, 0, 1, 0.95))

    @staticmethod
    def conditional_expected_profit(monetary_value, frequency, p, q, v):
        individual_weight = p * frequency / (p * frequency + q - 1)
        population_mean = v * p / (q - 1)
        return (1 - individual_weight) * population_mean + individual_weight * monetary_value

    @staticmethod
    def plot_gamma_gamma_sensitivity_sets(cohort_df: pd.DataFrame,
                                          sample_label_column: str,
                                          preferred_gamma_label: str = '',
                                          preferred_gamma_linewidth: float = 2.0,
                                          max_frequency: int = 4,
                                          suptitle_prefix: str = '',
                                          figsize: (int, int) = (10, 16),
                                          max_x: int = 100,
                                          max_y: int = 100):
        """
        Plots the gamma-gamma stability over multiple cohorts
        shows plot

        :param cohort_df: dataframe with entries for g-p, g-q, g-v and a labelling column.  each entry is [p, q, v]
        :param max_frequency: number of charts to do, maximum order frequency examined
        :param sample_label_column: name of column to use for labelling data for plot
        :param preferred_gamma_label: if the sample label column has this value, larger linewidth used for line
        :param preferred_gamma_linewidth: linewidth to use for the preferred beta label
        :param suptitle_prefix: prefix to the suptitle
        :param figsize: figure size for plot
        :param max_x: x upper limit in chart
        :param max_y: y upper limit in chart
        :return: none
        """
        fig, axes = plt.subplots(max_frequency, figsize=figsize)
        fig.subplots_adjust(top=0.95)
        x = np.linspace(0, 100, 1000)
        for frequency in range(0, max_frequency):
            ax = axes[frequency]
            for idx, cohort in cohort_df.iterrows():
                p = cohort['g-p']
                q = cohort['g-q']
                v = cohort['g-v']
                cohort_name = cohort[sample_label_column]
                if cohort_name == preferred_gamma_label:
                    linewidth = preferred_gamma_linewidth
                    linestyle = 'solid'
                else:
                    linewidth = 1.0
                    linestyle = 'dotted'
                population_mean_str = "{:0.2f}".format(v * p / (q - 1))
                p_rnd_str = "{:0.1f}".format(p)
                q_rnd_str = "{:0.1f}".format(q)
                v_rnd_str = "{:0.1f}".format(v)  # v only matters for population mean - not interesting otherwise

                y = np.array(list(map(partial(VPLifetimesHoldoutPlots.conditional_expected_profit,
                                              frequency=frequency,
                                              p=p,
                                              q=q,
                                              v=v), x)))
                _ = ax.plot(x, y,
                            label=f'{cohort_name}: ${population_mean_str} {p_rnd_str}; {q_rnd_str}; {v_rnd_str}',
                            linewidth=linewidth,
                            linestyle=linestyle)
            _ = ax.legend(loc='lower right', shadow=True, fontsize='small')
            _, ymax = ax.get_ylim()
            _ = ax.annotate(f'Prediction For Customers with\nFrequency={frequency}',
                            xy=(0, 0.9 * ymax),
                            fontsize=11)
            _ = ax.set_ylim(0, max_y)
            _ = ax.set_xticks(np.arange(0, max_x+1, 10))
            _ = ax.set_yticks(np.arange(0, max_y+1, 10))
            _ = ax.set_xlabel('Average Prior Spend', fontsize=11)
            _ = ax.set_ylabel('Predicted Spend Per Order', fontsize=11)
            if frequency == 0:
                _ = ax.annotate('Expected Spend is Invariant In Zero Case', xy=(0, 10), fontsize=11)
        plt.suptitle(f'{suptitle_prefix}\n'
                     'Gamma-Gamma Stability\nCalibration Experience and Predicted Order Spend Future Per Order')
        plt.tight_layout(rect=(0, 0, 1, 0.96))

    @staticmethod
    def mbg_conditional_probability_alive(days_since_order, frequency, T, r, alpha, a, b):
        recency = T-days_since_order
        #
        # next line may generate a RuntimeWarning
        # not sure under what circumstances and only seen when creating these plots
        return 1.0 / (1 + (a / (b + frequency)) * ((alpha + T) / (alpha + recency)) ** (r + frequency))

    @staticmethod
    def plot_beta_sensitivity_sets(cohort_df: pd.DataFrame,
                                   sample_label_column: str,
                                   preferred_beta_label: str = '',
                                   preferred_beta_linewidth: float = 2.0,
                                   max_frequency: int = 4,
                                   suptitle_prefix: str = '',
                                   model_name: str = 'ModifiedBetaGeo',
                                   assume_days_ago: int = 400,
                                   figsize: (int, int) = (10, 16)):
        """
        plot beta across various cohorts
        routes only to ModifiedBetaGeometric at present

        :param cohort_df: dataframe with entries for g-p, g-q, g-v and a labelling column.  each entry is [p, q, v]
        :param sample_label_column: the column used to label the data
        :param preferred_beta_label: if the sample label column has this value, larger linewidth used for line
        :param preferred_beta_linewidth: linewidth to use for the preferred beta label
        :param max_frequency: maximum number of subplots to show
        :param suptitle_prefix: prefix to the suptitle
        :param model_name: name to use in the title
        :param figsize: figure size to display
        :return: None
        """
        fig, axes = plt.subplots(max_frequency, figsize=figsize)
        fig.subplots_adjust(top=0.95)
        max_days_since_order = assume_days_ago * 2
        days_since_order = np.linspace(0, max_days_since_order, max_days_since_order)
        for frequency in range(0, max_frequency):
            ax = axes[frequency]
            for idx, cohort in cohort_df.iterrows():
                cohort_name = cohort[sample_label_column]
                if cohort_name == preferred_beta_label:
                    linewidth = preferred_beta_linewidth
                    linestyle = 'solid'
                else:
                    linewidth = 1.0
                    linestyle = 'dotted'
                r = cohort['b-r']
                alpha = cohort['b-alpha']
                a = cohort['b-a']
                b = cohort['b-b']
                r_rnd_str = "{:0.2f}".format(r)
                alpha_rnd = int(alpha)
                a_rnd_str = "{:0.2f}".format(a)
                b_rnd_str = "{:0.2f}".format(b)
                prob = np.array(list(map(partial(VPLifetimesHoldoutPlots.mbg_conditional_probability_alive,
                                                 frequency=frequency,
                                                 T=assume_days_ago,
                                                 r=r,
                                                 alpha=alpha,
                                                 a=a,
                                                 b=b), days_since_order)))
                _ = ax.plot(days_since_order,
                            prob,
                            label=f'{cohort_name}: a{a_rnd_str}; A{alpha_rnd}; b{b_rnd_str}; r{r_rnd_str}',
                            linewidth=linewidth,
                            linestyle=linestyle)
            _ = ax.legend(loc='lower left', shadow=True, fontsize=7)
            _ = ax.set_ylim(0, 1.0)
            _ = ax.annotate(f'{frequency+1} orders by customer', xy=(.8 * assume_days_ago, 0.9))
            _ = ax.set_yticks(np.arange(0, 1.01, 0.1))
            _ = ax.set_xlim(0, max_days_since_order)
            _ = ax.set_xticks(np.arange(0, max_days_since_order, 30))
            _ = ax.set_ylabel('Probability (alive)', fontsize=15)
        # only do this for bottom axis
        _ = ax.set_xlabel('Days Since Last Order', fontsize=15)
        plt.suptitle(f'{suptitle_prefix}\n'
                     f'{model_name} Sensitivity\n'
                     f'Calibration Period and Probability Alive\n'
                     f'Assume First Order {assume_days_ago} Days Ago')
        plt.tight_layout(rect=(0, 0, 1, 0.96))

    @staticmethod
    def plot_parameter_stability_pairplot(df: pd.DataFrame,
                                          sample_label_column: str,
                                          plot_title: str = 'Stability of Parameter Estimates Across Samples',
                                          sns_style: str = 'ticks',
                                          sns_palette: str = 'Set2',
                                          sns_scatter_size: float = 50.0,
                                          tight_layout_rect: (float, float, float, float) = (0, 0, 0.9, 0.96)):
        """
        Pass in dataframe with columns for variables to be plotted, and one
        categorical column which acts as label for each row

        :param df: dataframe with the data to be plotted
        :param sample_label_column: column to use as label
        :param plot_title: title to use for plot
        :param sns_style: style to use for plot
        :param sns_palette: palette to use for plot
        :param sns_scatter_size: size of dots in scatter plot
        :param tight_layout_rect: rectangle which creates room for suptitle and right legend
        """
        sns.set(style=sns_style)
        g = sns.PairGrid(df, hue=sample_label_column)
        g = g.map_lower(sns.scatterplot, palette=sns_palette, s=sns_scatter_size)
        g = g.map_upper(sns.scatterplot, palette=sns_palette, s=sns_scatter_size)
        _ = g.add_legend()
        plt.gcf().subplots_adjust(top=0.95)
        plt.suptitle(plot_title)
        plt.tight_layout(rect=tight_layout_rect)

    @staticmethod
    def _gen_linspace_df(max_spend: float = 150.0, obs_per_spend: float = 10.0):
        """
        create dataframe for reference line

        max_spend: draw linear space from 0 thru max_spend
        obs_per_spend: how many observations per spend (typically break each $1 into 10 intervals)
        """
        # reference line
        obs = int(obs_per_spend * max_spend + 1)
        a = np.linspace(0, max_spend, obs)
        a.shape = (obs, 1)
        b = np.concatenate((a, a), axis=1)
        return pd.DataFrame(b, columns=['x', 'y'])

    @staticmethod
    def plot_exp_spend_accuracy_scatter(purchasers: pd.DataFrame,
                                        xlabel: str,
                                        prediction_column: str,
                                        gt_column: str,
                                        max_spend: float = 150.0,
                                        plot_reference_line: bool = True,
                                        annotation_font_size: int = None,
                                        ax: plt.Axes = None):
        """
        draw subplot for purchase prediction accuracy

        purchasers: data to plot
        title: description of subplot
        xlabel: description of predictor (x axis is predicting, y-axis is gorund truth)
        prediction_column: column with prediction
        gt_column: column with ground truth
        plot_reference_line: boolean whether to plot the reference line
        annotation_font_size: if filled in, metrics are computed and annotated with this font size
        ax: Axes to plot (None means make new one)
        """
        # gamma-gamma: using Bayesian estimator for predicted holdout spend
        sns.set(rc={'figure.figsize': (9, 9)})
        _ = sns.regplot(x=prediction_column,
                        y=gt_column,
                        data=purchasers,
                        ax=ax,
                        scatter_kws={'s': 0.1})
        if annotation_font_size is not None:
            metrics = VPLifetimesHoldoutMetrics.get_gamma_gamma_expected_spend_metrics(purchasers,
                                                                                       prediction_column,
                                                                                       gt_column)
            ax.annotate(f'RMSE: {np.round(metrics["cond_mon_rmse"],2)}\n'
                        f'R Spearman: {np.round(metrics["cond_mon_rspearman"],3)}',
                        xy=(max_spend / 2.0, 0.9 * max_spend),
                        fontsize=annotation_font_size)
        if plot_reference_line:
            linear_space = VPLifetimesHoldoutPlots._gen_linspace_df(max_spend)
            _ = ax.plot(linear_space.x, linear_space.y, color='xkcd:red', linewidth=1.0, linestyle='dotted')
        ax.set_xlabel(xlabel)
        ax.set_ylabel('Holdout Mean Customer Spend')
        _ = ax.set_xlim(0, max_spend)
        _ = ax.set_ylim(0, max_spend)
        return ax
