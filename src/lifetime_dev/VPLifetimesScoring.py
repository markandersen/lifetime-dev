from tqdm import tqdm
from lifetimes.utils import summary_data_from_transaction_data
from lifetimes import GammaGammaFitter, ParetoNBDFitter, ModifiedBetaGeoFitter, BetaGeoBetaBinomFitter


class VPLifetimesScoring:
    """
    VPLifetimes Scoring takes a pandas order level dataframe and a model as input
    and from these it will provide scoring.
    """

    def __init__(self,
                 pd_df,
                 repeat_model_type,
                 repeat_params,
                 gamma_gamma_params,
                 observation_period_end,
                 customer_id_field_name,
                 monetary_value_field_name,
                 order_date_field_name,
                 forecast_days=None,  # want this to be [90, 180, 365] but mutable list issue
                 date_format='%Y-%m-%d'):
        """

        :param pd_df: pandas dataframe at order level
        :param repeat_model_type: type of repeat model to be used
        :param repeat_params: parameters for repeat model
        :param gamma_gamma_params: parameters for gamma-gamma model
        :param observation_period_end: when the period has ended (especially it may be later than last order)
        :param customer_id_field_name: name of field in pd_df
        :param monetary_value_field_name: name of field in pd_df
        :param order_date_field_name: name of field in pd_df
        :param forecast_days: number of days to forecast
        :param date_format: format of the date fields
        """
        if forecast_days is None:
            forecast_days = [90, 180, 365]
        self.df = pd_df
        assert repeat_model_type in VPLifetimesScoring.get_repeat_model_types(), \
            f'model type {repeat_model_type} in not in supported list'
        self.repeat_model_type = repeat_model_type
        self.repeat_params = repeat_params
        self.gamma_gamma_params = gamma_gamma_params
        self.observation_period_end = observation_period_end
        self.cust_field = customer_id_field_name
        self.money_field = monetary_value_field_name
        self.date_field = order_date_field_name
        self.forecast_days = forecast_days
        self.date_format = date_format
        # Create pre-fit models
        self.gg_model = None
        self.repeat_model = None
        self.instantiate_fit_models()
        # Create summary data.  First required to ensure field is type float
        self.df[monetary_value_field_name] = self.df[monetary_value_field_name].astype('float64')
        self.summary_df = summary_data_from_transaction_data(
            self.df,
            self.cust_field,
            self.date_field,
            self.money_field,
            self.date_format,
            observation_period_end,
            freq='D',
            freq_multiplier=1,
            include_first_transaction=False)

    @staticmethod
    def get_repeat_model_types():
        """
        Return: Possible model types for BGF
        """
        return ['BetaGeo', 'ModifiedBetaGeo', 'BetaGeoBetaBinom', 'ParetoNBD']

    def score_customers(self):
        self.summary_df['prob_alive'] = self.repeat_model.conditional_probability_alive(self.summary_df.frequency,
                                                                                        self.summary_df.monetary_value,
                                                                                        self.summary_df['T'])
        self.summary_df['conditional_expected_value_per_order'] = \
            self.gg_model.conditional_expected_average_profit(
                self.summary_df.frequency,
                self.summary_df.monetary_value
            )
        for forecast_duration in tqdm(self.forecast_days):
            assert isinstance(forecast_duration, int), 'Forecast days should be list of integers'
            self.summary_df[f'fcst_orders_{forecast_duration}'] = \
                self.repeat_model.conditional_expected_number_of_purchases_up_to_time(
                    forecast_duration,  # later optimization maybe can do in one pass
                    self.summary_df.frequency,
                    self.summary_df.recency,
                    self.summary_df['T']
            )
            #
            # Two reasons to forecast value by multiplying orders by conditional expected value per order
            #  a. The LTV functionality in Lifetimes is extremely slow with two calculations per month
            #     due to the use of discounting. We do not need discounting
            #  b. The LTV functionality cannot handle fractional months.  Multiplying this logic does
            #     handle fractional months.  Handling of whole months is confirmed to match the Lifetimes
            #     code exactly, and the fractional months have appropriate intermediate values.
            #
            self.summary_df[f'fcst_value_{forecast_duration}'] = \
                self.summary_df[f'fcst_orders_{forecast_duration}'] * \
                self.summary_df['conditional_expected_value_per_order']
        return self.summary_df.reset_index()  # move shopper to a named column

    def instantiate_fit_models(self):
        self.gg_model = GammaGammaFitter()
        self.gg_model.params_ = self.gamma_gamma_params

        assert self.repeat_model_type == 'ModifiedBetaGeo', 'Only ModifiedBetaGeo supported'
        self.repeat_model = ModifiedBetaGeoFitter()
        self.repeat_model.params_ = self.repeat_params
        self.repeat_model.predict = self.repeat_model.conditional_expected_number_of_purchases_up_to_time
