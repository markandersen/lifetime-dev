import os
import logging
import pandas as pd
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis
from pathlib import Path
from kedro.framework.context import load_context
pd.options.display.max_columns = 50

#
# - General Processing for Fitting -- note that 'W' or 'D' can be used for frequency below
#
# Also model_type can be replaced if one needs to measure BetaGeo
#
log_dir = os.path.join('..', '..', 'logs', 'temp')
log_filename = os.path.join(log_dir, 'model-fitting.log')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
if os.path.exists(log_filename):
    os.remove(log_filename)
logging.basicConfig(filename=log_filename,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger('__name__')

project_path = Path(os.getcwd()).parents[1].resolve()  # 2 levels up from this file
context = load_context(project_path)

countries = ['IE', 'ES']
countries_file_name = '-'.join(countries)
start_date = '2010-07-01'
end_date = '2020-08-01'

orders_df = context.catalog.load("ie_es_2010_2020")
model_type = 'ModifiedBetaGeoFit'
country_field = 'ACCOUNT_CREATION_COUNTRY'
observe_precovid_date = '2020-02-28'
order_date_field = 'ORDER_DATE'
param_fields = ['b-r', 'b-alpha', 'b-a', 'b-b', 'g-p', 'g-q', 'g-v']
fit_results = {}
rows = []
freq = 'D'
model_type = 'ModifiedBetaGeo'
freq_multiplier = 1
for country in tqdm(countries):
    country_df = orders_df[orders_df[country_field] == country]

    # one country for now
    country_results = {}
    for obs_date in tqdm([end_date, observe_precovid_date]):
        vp_lifetime_analysis = VPLifetimesAnalysis(VPLifetimesData(country_df))
        # VPLifetimesData will truncate on observation date when fitting order data
        params, bb_params, gg_params = vp_lifetime_analysis.fit_order_data(obs_date,
                                                                           prediction_model_penalizer=0.0,
                                                                           gg_penalizer=0.0,
                                                                           model_type=model_type,
                                                                           freq=freq,
                                                                           freq_multiplier=freq_multiplier)
        country_results[obs_date] = params
        row = [country, obs_date]
        for key in param_fields:
            row.append(params[key])
        rows.append(row)
    fit_results[country] = country_results

print(fit_results)
results_df = pd.DataFrame(rows, columns=['country', 'observation_date'] + param_fields)

# one time population of data set from results
results_df.to_csv(os.path.join('..', '..', 'data', '07_model_output', f'fit_models-freq_{freq}-{countries_file_name}-{start_date}-{end_date}.csv'))

results_df
