import seaborn as sns
import os
import logging
import pandas as pd
import numpy as np
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis
pd.options.display.max_columns = 50

def fill_omitted_calibration_monetary_values(df, monetary_value_field, population_mean):
    """
    Fills in population mean value for those observations lacking data
    """
    df.loc[df[monetary_value_field] == 0, monetary_value_field] = population_mean

#
#- General Processing
#
log_dir = os.path.join('..', '..', 'logs', 'temp')
log_filename = os.path.join(log_dir, 'VPLifetimes.log')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
if os.path.exists(log_filename):
    os.remove(log_filename)
logging.basicConfig(filename=log_filename,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger('__name__')


model_types = VPLifetimesAnalysis.get_model_types()  # static method
# We find that performance is too slow to use BetaGeoBetaBinomFit and ParetoNBD is not fast either
model_types = list(set(model_types) - {'ParetoNBDFit', 'BetaGeoBetaBinomFit'})

#
# - One specific source file
#

# src_file = 'VPLifetimes--orders--ES--2015-07-01-2020-07-01--2015-07-01-2020-07-01.0.1.pkl'
# slightly smaller file

countries = 'IE-ES'
start_date = '2010-07-01'
end_date = '2020-07-21'
version = '0.11'
src_file = f'VPLifetimes--orders--{countries}--{start_date}-{end_date}--{start_date}-{end_date}.{version}.pkl'
src = os.path.join('..', '..', 'data', '01_raw', src_file)
orders_df = pd.read_pickle(src)
model_type = 'ModifiedBetaGeoFit'

# analyze LTV over time
ltvs = {}
for obs_date in ['2020-06-30']:  # tqdm(['2016-12-31', '2017-12-31', '2018-12-31', '2019-12-31', '2020-06-30']):
    vp_lifetime_data = VPLifetimesData(orders_df)
    vp_lifetime_analysis = VPLifetimesAnalysis(vp_lifetime_data)

    summary_data = vp_lifetime_data.get_summary_data_from_transaction(obs_date)
    print(f'Length of summary data is {len(summary_data)}')
    summary_data = summary_data[summary_data.frequency>0]
    print(f'After trim zero frequency len is {len(summary_data)}')
    vp_lifetime_analysis.reset_order_model()
    vp_lifetime_analysis.order_model.fit(summary_data.frequency,
                                         summary_data.recency,
                                         summary_data['T'])
    bgf_params = vp_lifetime_analysis.order_model._unload_params("r", "alpha", "a", "b")
    ggf_model = vp_lifetime_analysis.gamma_gamma_fit(summary_data.frequency,
                                                      summary_data.monetary_value)
    ggf_params = ggf_model._unload_params("p", "q", "v")
    clv_set = vp_lifetime_analysis.gg_model.customer_lifetime_value(
        vp_lifetime_analysis.order_model,
        summary_data.frequency,
        summary_data.recency,
        summary_data['T'],
        summary_data.monetary_value,
        time=36,
        discount_rate=0.0
    )
    frequent_shopper_count = len(clv_set)
    total_clv = sum(clv_set)
    clv_per_shopper = np.round(total_clv / frequent_shopper_count, 2)
    ltvs[obs_date] = {
        'shoppers': frequent_shopper_count,
        'ltv': total_clv,
        'ltv_per_shopper': clv_per_shopper,
        'data': clv_set.reset_index(),
        'ggf_params': ggf_params,
        'bgf_params': bgf_params
    }

print('high level results')
# print out results
for key in ltvs:
    print(f'{key}: ${ltvs[key]["ltv"]} over {ltvs[key]["shoppers"]} shoppers = {ltvs[key]["ltv_per_shopper"]} '
          f'ggf: {ltvs[key]["ggf_params"]} bgf: {ltvs[key]["bgf_params"]}')

# get some shoppers
#first_year = '2016-12-31'
#sample_customers = ltvs[first_year]['data'][:40].reset_index()
#last_year = '2019-12-31'
#recent_customers = ltvs[last_year]['data'].reset_index()
#df = sample_customers.merge(recent_customers, on='SHOPPER_ID')
#df[df.clv_y>df.clv_x]


# cohort analysis
orders_df['cohort_year'] = orders_df.FIRST_ORDER_DATETIME.dt.year
# find shopper cohorts
shopper_cohort = orders_df.groupby('SHOPPER_ID').cohort_year.min().reset_index()
print(f'shoppers evaluated {len(shopper_cohort)}')
cohort_results = {}


for obs_year in tqdm(np.arange(2016, 2020)):
    obs_date = f'{obs_year}-12-31'
    shopper_cohort = shopper_cohort.merge(ltvs[obs_date]['data'][['SHOPPER_ID', 'clv']],
                                          how='left',
                                          on='SHOPPER_ID')
    shopper_cohort.rename(columns={'clv': f'clv_{obs_year}'}, inplace=True)

# shoppers with any value will have value in 2019
valued_shoppers = shopper_cohort[shopper_cohort.clv_2019.isnull() == False]
print(valued_shoppers[:20][['cohort_year', 'clv_2016', 'clv_2017', 'clv_2018', 'clv_2019']])


def clv_compare_years(shopper_df: pd.DataFrame,
                      start_year: int,
                      end_year: int,
                      min_cohort: int = 2015,
                      max_cohort: int = 2019):
    """
    Compares collective performance for two years
    :param shopper_df: pandas dataframe with fields for cohort_year and clv_yyyy for years
    :param start_year: first year to look at
    :param end_year: year to compare
    :param min_cohort: first year to examine cohort performance
    :param max_cohort: last year to examine cohort performance
    """
    # find shoppers for whom there is data in the end year
    start_year_field = f'clv_{start_year}'
    end_year_field = f'clv_{end_year}'
    shopper_df = shopper_df[shopper_df[end_year_field].isnull() == False]
    for cohort in np.arange(min_cohort, max_cohort+1, 1):
        cohort_df = shopper_df[shopper_df.cohort_year == cohort]
        end_year_len = len(cohort_df)
        cohort_with_start_value = cohort_df[cohort_df[start_year_field].isnull() == False]
        cohort_without_start_value = cohort_df[cohort_df[start_year_field].isnull() == True]
        print(f'Year {cohort} has {len(cohort_without_start_value)} which are null')
        start_year_performance = int(cohort_with_start_value[start_year_field].sum())
        start_year_len = len(cohort_with_start_value)
        end_year_performance = int(cohort_with_start_value[end_year_field].sum())
        print(f'Year {cohort} has total value {start_year_performance}  in {start_year} over {start_year_len} shoppers '
              f'and {end_year_performance} in {end_year} over {end_year_len} shoppers for the year')
        improve = cohort_with_start_value[cohort_with_start_value[start_year_field] <
                                          cohort_with_start_value[end_year_field]][end_year_field].count()
        decline = cohort_with_start_value[cohort_with_start_value[start_year_field] >
                                          cohort_with_start_value[end_year_field]][end_year_field].count()
        print(f'Count of shoppers improving {improve} and declining {decline}')

# report on value
clv_compare_years(valued_shoppers, 2018, 2019)




assert False, 'stop'

# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

#
# shorten run
#orders_df = orders_df.sample(150000)


# Key calibration off model_type and holdout_date
model_types_used = ['BetaGeoFit']  # model_types

holdout_dates = ['2017-12-31',
                 '2018-03-31',
                 '2018-06-30',
                 '2018-09-30',
                 '2018-12-31',
                 '2019-03-31',
                 '2019-06-30',
                 '2019-09-30',
                 '2019-12-31',
                 '2020-03-31']
holdout_dates = ['2017-12-31',
                 '2018-12-31',
                 '2019-06-30',
                 '2019-12-31',
                 '2020-03-31']

holdout_dates = ['2018-12-31', '2019-06-30', '2019-12-31']
study_dates = ['2019-06-30', '2019-12-31', '2020-06-30']
include_plots = False
analysis_results = {}

for study_date in tqdm(study_dates):
    # VPLifetimesData will make a local copy and truncate at study date
    vp_lifetime_data = VPLifetimesData(orders_df)
    vp_lifetime_analysis = VPLifetimesAnalysis(vp_lifetime_data)

    for holdout_date in tqdm(holdout_dates):
        if holdout_date >= study_date:
            print(f'Skipping holdout {holdout_date} for study date {study_date}')
            continue
        #
        dataset_descriptor = f'{model_type}:{country}:{start_date}-{end_date} holdout {holdout_date}'
        # Create object for order prediction
        vplop = VPLifetimesOrderPredictAnalyze(vp_lifetime_data,
                                               calibration_period_end=holdout_date,
                                               observation_period_end=study_date)
        data = vplop.calibration(vp_lifetime_analysis, model_type, 0.5)
        analysis_results[(model_type, study_date, holdout_date)] = vplop.calc_order_predictions(vp_lifetime_analysis)

        if include_plots:
            plots = VPLifetimePlots(vplop.calholddata, holdout_date)
            print('------ plotting lifetime charts----------')
            plots.plot_default_lifetimes_charts_prob_alive(vp_lifetime_analysis.order_model, dataset_descriptor)
            print('plotting custom charts ----------')
            plots.plot_custom_holdout_evaluations(dataset_descriptor)

#
# print out the results
#
for key in analysis_results:
    frequency_stats, monetary_stats, _ = analysis_results[key]
    print(f'{key}: {frequency_stats}, {monetary_stats}')

















































assert False, 'stopping here'




model_types_used = model_types  # ['BetaGeoFit']  # model_types
holdout_calfits = {}
holdout_scores = {}
for holdout_date in ['2018-12-31']: # , '2019-07-01', '2019-12-31']:
  print(f'Modeling holdout date {holdout_date}')
  cb_data = get_calibration_data(vp_lifetime_data, holdout_date)
  cb_small = cb_data  #  cb_data.sample(50000)  # optional take subset of very large sets
  calfits = {}
  scores = {}
  for model_type in model_types_used:
    print(f'Scores for {model_type}')
    calfits[model_type] = plot_calibration(cb_small, vp_lifetime_analysis, model_type, .1, suppress=False)
    summary, scores[model_type]  = add_model_predictions(cb_small, calfits[model_type])
  # save in larger dictionary
  holdout_calfits[holdout_date] = calfits
  holdout_scores[holdout_date] = scores
  



sum_short =summary[summary.frequency_cal<10]
g = sns.FacetGrid(sum_short, row='frequency_cal', aspect=15, height=.5)
g.map(sns.kdeplot, "model_predictions")
g.map(plt.axhline, y=0, lw=2, clip_on=False)
g.fig.subplots_adjust(hspace=-.25)
g.set_titles("")
#g.set(yticks=[])
#g.despine(bottom=True, left=True)
plt.show()

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Appendix

# COMMAND ----------


holdout_dates = ['2019-12-31']  #  ['2018-07-01', '2018-12-31', '2019-07-01', '2019-12-31', '2020-03-30']:
#model_types =
# dict for later use
bgfs = {}
for holdout_date in ['2018-07-01', '2018-12-31', '2019-07-01', '2019-12-31', '2020-03-30']:
  print(f'Predictions for holdout starting {holdout_date}')
  ch = vp_lifetime_data.get_calibration_holdout(holdout_date)
  #rename due to hardcoding of columns in lifetimes
  ch.rename(columns={'frequency_of_repeat_cal': 'frequency_cal',
                     'frequency_of_repeat_holdout': 'frequency_holdout',
                     'Tval_cal': 'T_cal'}, inplace=True)
  vp_lifetime_analysis.beta_fit()

  bgf = vp_lifetime_analysis.get_bgf()
  bgfs[holdout_date] = bgf  
  bgf.fit(ch['frequency_cal'], ch['recency_cal'], ch['T_cal'])
  ax = plot_calibration_purchases_vs_holdout_purchases(bgf, ch, kind='frequency_cal')
  ax.set_title(f'Calibration for period through {holdout_date}')
  plt.show()

# COMMAND ----------

# last bgf - plot period transactions
# timing: 1.24 minutes for ES data
from lifetimes.plotting import plot_period_transactions
plot_period_transactions(bgf)

# COMMAND ----------

plot_period_transactions(bgf)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Visualize Lifetimes

# COMMAND ----------

#
# Configure
#
visualize_results = True

if visualize_results:
  vp_lifetime_analysis.calculate_conditional_probability_alive(verbose=True)
  vp_lifetime_analysis.plot_conditional_probability_alive(plot_split_hist=True)
  vp_lifetime_analysis.plot_frequency_recency_matrix()
  vp_lifetime_analysis.calculate_plot_expected_purchases()


# COMMAND ----------

import seaborn as sns
print(sns.__version__)
assert sns.__version__=='0.10.1', 'Seaborn 0.10.1 required (or higher)'

sns.stripplot(x='decile', y='Tval', data=single_analysis.sample(5000, replace=True), jitter=0.3, size=1)  
# jitter, sample and size used for aesthetic reasons

# COMMAND ----------

assert False, "Specific Country Applications Run Manually"

# COMMAND ----------

# MAGIC %md
# MAGIC ### Country Applications

# COMMAND ----------

lt_jp = {
  'name': 'Japan 2016+',
  'test_cohort_start_date': '2016-01-01',
  'test_cohort_end_date': '2020-06-17',
  'test_countries': ['JP'],
  }

lt_gb = {
  'name': 'GB 2016+',
  'test_cohort_start_date': '2016-01-01',
  'test_cohort_end_date': '2020-06-17',
  'test_countries': ['GB'],
  }

lt_fr = {
  'name': 'FR 2016+',
  'test_cohort_start_date': '2016-01-01',
  'test_cohort_end_date': '2020-06-17',
  'test_countries': ['FR'],
  }

# COMMAND ----------

import uuid
def generate_analyze_lifetime(snowflake, lt_model_dict_list, model_type='ModifiedBetaGeoFit', visualize_results=False, verbose=False):
  """
  generates lifetime data and analyzes for a dictionary describing case
  
  snowflake: instance for communicating with snowflake
  lt_model_dictlist: list of lifetimes dictionary for running a model
  model_type: type of model which will be used (ModifiedBetaGeoFit or BetaGeoFit)
  visualize_results: whether to show diagrams for this model
  verbose: whether to output additional debugging information
  """
  print(f'verbose is {verbose}')
  for cntr, lt_model_dict in enumerate(lt_model_dict_list):
    model_id = str(uuid.uuid4())
    print(f'For {lt_model_dict["name"]} generating data model {model_id}')
    vp_lifetime_data = VPLifetimeData(model_id, countries=lt_model_dict["test_countries"],
                                      sample_start_date=lt_model_dict['test_cohort_start_date'],
                                      sample_end_date=lt_model_dict['test_cohort_end_date'])
    orders_sdf = vp_lifetime_data.get_orders(debug=verbose)
    orders_pdf = vp_lifetime_data.get_recency_frequency_t_pandas()
    # refactored: next line now wont work
    # vp_lifetime_analysis = VPLifetimesAnalysis(orders_pdf, vp_lifetime_data, model_type=model_type)
   # vp_lifetime_analysis.write_snowflake_alive_purchases(snowflake, verbose=verbose)
    #if visualize_results:
    #  vp_lifetime_analysis.calculate_conditional_probability_alive(verbose=verbose)
    #  vp_lifetime_analysis.plot_conditional_probability_alive(plot_split_hist=True)
    #  vp_lifetime_analysis.plot_frequency_recency_matrix()
    #  vp_lifetime_analysis.calculate_plot_expected_purchases()
  if verbose:
    print('done')

# COMMAND ----------

if False:
  # not yet working: no JP shoppers are in shopper table we depend on
  generate_analyze_lifetime(snowflake, [lt_jp], visualize_results=True)
  # GB also not working
  generate_analyze_lifetime(snowflake, [lt_gb], visualize_results=True, verbose=True)


# COMMAND ----------

generate_analyze_lifetime(snowflake, [lt_fr], visualize_results=True, verbose=True)
print('ran cell')

# COMMAND ----------

# MAGIC %md
# MAGIC ## Appendix

# COMMAND ----------

assert False, 'appendix'

# COMMAND ----------

# clean up as needed 
# snowflake.table_drop(database='STAGING', schema='DNA', name='VPLT_SHOPPER')

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ### ie_shop_test - secondary, only to show what happens if we do not use all orders

# COMMAND ----------

test_shoppers = False

if test_shoppers:

  shoppers_df = vp_lifetime_test.get_shoppers(debug=True)
  ie_shoppers_pdf = vp_lifetime_test.get_recency_frequency_t_pandas()
  ie_shoppers_pdf.info()
  ie_shop_test = VPLifetimesAnalysis(ie_shoppers_pdf)
  ie_shop_test.calculate_conditional_probability_alive(verbose=True)
  ie_shop_test.plot_conditional_probability_alive()
  ie_shop_test.plot_frequency_recency_matrix()
  ie_shop_test.calculate_plot_expected_purchases()

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ### Analyze: Shoppers and Orders based Results are Different - Why?

# COMMAND ----------

if test_shoppers:
  matching_pdf = ie_orders_pdf.merge(ie_shoppers_pdf, how='outer', indicator=True, left_index=True, right_index=True, suffixes=['_ord','_shop'])
  matching_pdf.info()
  matching_pdf.head(10)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Mismatches: typically shoppers shows more orders than the actual count of orders on distinct days 
# MAGIC (summary from transaction data collapses by unit of time)

# COMMAND ----------

#
if test_shoppers:
  print(f'match frequency count: {len(matching_pdf[matching_pdf.frequency_of_repeat_ord==matching_pdf.frequency_of_repeat_shop])}')
  print(f'mismatch frequency count: {len(matching_pdf[matching_pdf.frequency_of_repeat_ord!=matching_pdf.frequency_of_repeat_shop])}')
  mismatches = matching_pdf[matching_pdf.frequency_of_repeat_ord!=matching_pdf.frequency_of_repeat_shop]
  mismatches

# COMMAND ----------

# MAGIC %md
# MAGIC ### Hypothesis: Mismatches caused by people who have multiple orders on same day

# COMMAND ----------

if test_shoppers:
  orders_pdf = orders_df.toPandas()
  shoppers_by_order_date = orders_pdf.groupby(['SHOPPER_ID', 'order_date']).ORDER_ID.count()
  len(shoppers_by_order_date)

# COMMAND ----------

if test_shoppers:
  multi_purchase_dates = shoppers_by_order_date[shoppers_by_order_date>1]
  print(len(multi_purchase_dates))
  multi_purchase_dates

# COMMAND ----------

# MAGIC %md
# MAGIC #### 19 of 20 shopper mismatches are due to multiple on order date
# MAGIC 
# MAGIC * What is left?

# COMMAND ----------

if test_shoppers:
  mmdate_pdf = multi_purchase_dates.to_frame().reset_index()  # multi-index so important to reset_index()
  mmdate_pdf

# COMMAND ----------

if test_shoppers:
  if '_merge' in mismatches.columns:
    mismatches.drop(columns=['_merge'], inplace=True)
  left_out = mismatches.merge(mmdate_pdf, left_index=True, right_on='SHOPPER_ID', how='left', indicator=True)
  left_out[left_out._merge=='left_only']

# COMMAND ----------

# MAGIC %md
# MAGIC ### Further investigation: this order has first order date in Jan 2019 but the system counts is_new in June 2020 but total order count 2

# COMMAND ----------



# COMMAND ----------

# MAGIC %md 
# MAGIC #### Appendix

# COMMAND ----------

# MAGIC %md
# MAGIC ### Test further functions

# COMMAND ----------

# MAGIC %md
# MAGIC ### Ireland Shoppers

# COMMAND ----------

a = VPLifetime(countries='IE', sample_start_date='2018-01-01', sample_end_date='2020-06-08')
sdf = a.get_shopper_data(debug=True)

# COMMAND ----------

# MAGIC %md
# MAGIC # Attention: About 7 rows are dropped by the Spark dataframe when saved and retrieved

# COMMAND ----------

# MAGIC %md
# MAGIC #### Basic Frequency/Recency using the BG/NBG model
# MAGIC * https://lifetimes.readthedocs.io/en/latest/Quickstart.html

# COMMAND ----------

t = 1
#: Note: i had to cast as type float64 to make this work in databricks
modeled_customers['predicted_purchases'] = \
  bgf.conditional_expected_number_of_purchases_up_to_time(t, 
                                                          modeled_customers.frequency_of_repeat.astype('float64'),
                                                          recency=modeled_customers.recency.astype('float64'),
                                                          T=modeled_customers.Tval)
modeled_customers.sort_values(by='predicted_purchases', ascending=False).head(5)



# COMMAND ----------

from lifetimes.plotting import plot_period_transactions
plot_period_transactions(bgf)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Ranking Customers

# COMMAND ----------

for t in [30, 90, 180, 365]:
  modeled_customers['predicted_purchases'] = \
    bgf.conditional_expected_number_of_purchases_up_to_time(t,
                                                            modeled_customers.frequency_of_repeat.astype('float64'),
                                                            recency=modeled_customers.recency.astype('float64'),
                                                            T=modeled_customers.Tval)
  print(f'For {t} days in future, predicted purchases:')
  print(modeled_customers.sort_values(by='predicted_purchases', ascending=False).head(10))
  print('-------------')



# COMMAND ----------

from lifetimes.plotting import plot_period_transactions
plot_period_transactions(bgf)

# COMMAND ----------

modeled_customers['prob_alive'] =   bgf.conditional_probability_alive(frequency=modeled_customers.frequency_of_repeat.astype('float64'),
                                                     recency=modeled_customers.recency.astype('float64'),
                                                     T=modeled_customers.Tval)

palive_customers = modeled_customers[modeled_customers.prob_alive<1]


# COMMAND ----------

# Processing
es_bgf = BetaGeoFitter(penalizer_coef=0.01)
es_bgf.fit(frequency=es_pdf.frequency_of_repeat, 
        recency=es_pdf.recency,
        T=es_pdf.Tval, verbose=True)
print(es_bgf)
es_pdf['prob_alive'] =   es_bgf.conditional_probability_alive(frequency=es_pdf.frequency_of_repeat.astype('float64'),
                                                     recency=es_pdf.recency.astype('float64'),
                                                     T=es_pdf.Tval)
print(f'Num pegged at prob_alive=1: {len(es_pdf[es_pdf.prob_alive==1])}')
print(f'Num realistic prob_alive: {len(es_pdf[es_pdf.prob_alive<1])}')



