import pandas as pd
from lifetimes.utils import summary_data_from_transaction_data, calibration_and_holdout_data


class VPLifetimesData:

    def __init__(self,
                 transactions_pandas_df: pd.DataFrame,
                 id_field='SHOPPER_ID',
                 date_field='ORDER_DATE',
                 monetary_value_field='AGP_BUDGET_USD',  # was TOTAL_BOOKINGS_BUDGET_USD
                 date_format='%Y-%m-%d'
                 ):
        """
        Manages Lifetimes data set.
        Cleans data to remove non-positive monetary value
        Can create this data set from any transaction dataframe, which perhaps is created by having
        Databricks save it to a file and loading it from the file.

        :param transactions_pandas_df: the raw transaction data 
        :param datetime_format: format of the date strings
        """
        self.orders_df = transactions_pandas_df
        self.id_field = id_field
        self.date_string_field = date_field
        self.monetary_value_field = monetary_value_field
        self.date_format = date_format
        # monetary value must be a float, and float64 needed for optimal algorithm speed
        self.orders_df[monetary_value_field] = self.orders_df[monetary_value_field].astype('float64')
        # do not allow any observations with zero or negative bookings
        self.orders_df = self.orders_df[self.orders_df[monetary_value_field] > 0]

    def get_calibration_holdout(self,
                                calibration_period_end: str,
                                observation_period_end: str = None,
                                freq: str = 'D',
                                freq_multiplier: int = 1) -> pd.DataFrame:
        """
        gets the calibration and holdout set for the data

        :param calibration_period_end: separate calibration from holdout period
        :param observation_period_end: passes through to truncate data if provided
        :param freq: frequency freq to pass to lifetimes ('D', 'M')
        :param freq_multiplier: pass to lifetimes
        :return: DataFrame with the standard calibration and holdout columns
        """
        # logger = logging.getLogger(__name__)
        df = calibration_and_holdout_data(self.orders_df,
                                          customer_id_col=self.id_field,
                                          datetime_col=self.date_string_field,
                                          calibration_period_end=calibration_period_end,
                                          observation_period_end=observation_period_end,
                                          monetary_value_col=self.monetary_value_field,
                                          datetime_format=self.date_format,
                                          freq=freq,
                                          freq_multiplier=freq_multiplier)
        for column in ['recency_cal', 'T_cal', 'frequency_cal', 'frequency_holdout']:
            df[column] = df[column].astype('float64')  # if one uses float32 model may not converge

        # Not going to remove frequency_cal=0 records: that is deferred until someone calls gamma-gamma
        # logger.info(f'Removing frequency_cal == 0 records from calibration_holdout to satisfy gamma-gamma')
        # df = df[df.frequency_cal > 0]
        return df

    def get_summary_data_from_transaction(self,
                                          observation_period_end: str = None,
                                          freq: str = 'D',
                                          freq_multiplier: int = 1
                                          ) -> pd.DataFrame:
        """
        gets the summary data from transaction data for the data in this instance

        :param observation_period_end: can apply to this call to cut off data after obs_date
          Note: the Spark date type does not work here, it is a string
        :param freq: frequency freq to pass to lifetimes ('D', 'M')
        :param freq_multiplier: pass to lifetimes
        :return: dataframe with the data, will be indexed on SHOPPER_ID
        """
        df = summary_data_from_transaction_data(transactions=self.orders_df,
                                                customer_id_col=self.id_field,
                                                datetime_col=self.date_string_field,
                                                observation_period_end=observation_period_end,
                                                monetary_value_col=self.monetary_value_field,
                                                datetime_format=self.date_format,
                                                freq=freq,
                                                freq_multiplier=freq_multiplier)
        # should ensure correct type for all critical columns
        for column in ['recency', 'T', 'frequency']:
            df[column] = df[column].astype('float64')  # if one uses float32 model may not converge
        return df

