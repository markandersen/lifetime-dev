import logging
import pandas as pd
import numpy as np
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis


class VPLifetimesOrderPredictAnalyze:
    """
    Be aware that negative monetary value orders are not workable in Lifetimes gamma-gamma
    to some extent cleaning is implemented in VPLifetimeData
    """

    def __init__(self,
                 lifetimes_data: VPLifetimesData,
                 calibration_period_end: str,
                 observation_period_end: str = None,
                 freq: str = 'D',
                 freq_multiplier: int = 1):
        """

        lifetimes_data: raw source of data
        calibration_period_end: parameter for get_calibration_holdout
        observation_period_end: parameter for get_calibration_holdout
        freq: parameter for get_calibration_holdout
        freq_multiplier: parameter for get_calibration_holdout
        """

        self.lifetimes_data = lifetimes_data
        self.holdout_date = calibration_period_end
        self.observation_period_end = observation_period_end
        #
        self.holdout_frequencies = None  # to be filled in by calc_order_predictions()
        self.frequency_stats = None  # to be filled in by calc_order_predictions()
        self.monetary_stats = None  # to be filled in by calc_order_predictions()
        self.gg_fit_model = None  # when GG is fit against calibration put here
        #
        self.calholddata = self.lifetimes_data.get_calibration_holdout(calibration_period_end,
                                                                       observation_period_end,
                                                                       freq=freq,
                                                                       freq_multiplier=freq_multiplier)

    def calibration(self,
                    lifetimes_analysis: VPLifetimesAnalysis,
                    model_type: str,
                    predict_penalizer: float = 0.0,
                    gg_penalizer: float = 0.0) -> pd.DataFrame:
        """
        do calibration

        lifetime_analysis: lifetime analysis object
        model_type: type of model: used for lifetime analysis and printing string
        penalizer: passed into model as created

        Returns tuple: (all parameters from fit models, predict model parameters only, gamma-gamma parameters only)
        """
        # Gets order model setup and penalizer
        log = logging.getLogger(__name__)

        log.info(f'Trying to fit order model with length {len(self.calholddata)}')

        fit_params, order_params, gg_params = lifetimes_analysis.fit(self.calholddata,
                                                                     'frequency_cal',
                                                                     'recency_cal',
                                                                     'T_cal',
                                                                     'monetary_value_cal',
                                                                     predict_penalizer,
                                                                     gg_penalizer,
                                                                     model_type)
        #
        # apply fit to entire data setp
        #
        self.calholddata['conditional_bookings'] = \
            lifetimes_analysis.gg_model.conditional_expected_average_profit(
                self.calholddata['frequency_cal'],
                self.calholddata['monetary_value_cal'])
        return fit_params, order_params, gg_params

    def calc_order_predictions(self,
                               lifetimes_analysis: VPLifetimesAnalysis,
                               max_holdout_frequency: int = None) -> ():
        """
        add in model predictions to the dataframe, exactly as lifetimes does under the hood

        max_holdout_frequency: an integer, typically in the 5-10 range.  If supplied then statistics will
          also be computed for each holdout_frequency up to the max.  Setting this value will
          make this function take longer to execute due to additional computations.
        maximum frequency to use when printing out stats.  however all frequencies are considered for MSE.

        summary statistics include:
        MSE: mean square error between prediction and holdout
        mean_error: mean error between prediction and holdout (what the calibration plot is currently showing)
        contribution_to_error: breakdown of how much is being contributed by each frequency
        """
        log = logging.getLogger(__name__)
        caldf = self.calholddata  # local short hand, deep copy
        pd.set_option('display.max_columns', 20)  # ensure the 6 column df will be printed
        duration_holdout = caldf.iloc[0]["duration_holdout"]
        caldf["model_predictions"] = lifetimes_analysis.order_model.conditional_expected_number_of_purchases_up_to_time(
            duration_holdout,
            caldf["frequency_cal"],
            caldf["recency_cal"],
            caldf["T_cal"])

        caldf['model_prediction_bookings'] = caldf.model_predictions * caldf.conditional_bookings
        # log.info('model_predictions: frequency and bookings')
        # log.info(caldf.model_predictions.describe())
        # log.info(caldf.model_prediction_bookings.describe())

        # statistics for repeat order predictions
        caldf["frequency_square_error"] = (caldf.model_predictions - caldf.frequency_holdout) ** 2
        caldf['frequency_square_error_zero_guess'] = caldf.frequency_holdout ** 2
        rho_df = caldf[['frequency_holdout', 'model_predictions']].corr(method='spearman')
        self.frequency_stats = {
            'mse': np.round(caldf.frequency_square_error.mean(), 3),
            'zero_mse': np.round(caldf.frequency_square_error_zero_guess.mean(), 3),
            'spearman_rho': np.round(rho_df.loc['model_predictions', 'frequency_holdout'], 3)
        }

        # statistics for monetary predictions
        caldf["monetary_square_error"] = (caldf.model_prediction_bookings - caldf.monetary_value_holdout) ** 2
        caldf['monetary_square_error_zero_guess'] = caldf.monetary_value_holdout ** 2
        rho_df = caldf[['monetary_value_holdout', 'model_prediction_bookings']].corr(method='spearman')
        self.monetary_stats = {
            'mse': np.round(caldf.monetary_square_error.mean(), 3),
            'zero_mse': np.round(caldf.monetary_square_error_zero_guess.mean(), 3),
            'spearman_rho': np.round(rho_df.loc['model_prediction_bookings', 'monetary_value_holdout'], 3)
        }
        #
        # detailed statistics for holdout frequencies, if max_holdout_frequency is not None
        #
        if max_holdout_frequency is None:
            self.holdout_frequencies = None
        else:
            subset = caldf[caldf.frequency_holdout <= max_holdout_frequency]
            holdout_frequencies = subset.groupby(['frequency_holdout']).agg({'frequency_square_error': np.mean,
                                                                             'model_predictions': np.mean,
                                                                             'T_cal': 'count',
                                                                             'frequency_square_error_zero_guess': np.mean}).reset_index()
            holdout_frequencies.rename(columns={'T_cal': 'obs'}, inplace=True)
            holdout_frequencies['cterr'] = holdout_frequencies.frequency_square_error * holdout_frequencies.obs
            holdout_frequencies['cum_cterr'] = holdout_frequencies.cterr.cumsum()
            holdout_frequencies[
                'mean_error'] = holdout_frequencies.frequency_holdout - holdout_frequencies.model_predictions
            holdout_frequencies[
                'cterr_z'] = holdout_frequencies.frequency_square_error_zero_guess * holdout_frequencies.obs
            holdout_frequencies['cum_cterr_z'] = holdout_frequencies.cterr_z.cumsum()
            holdout_frequencies = holdout_frequencies.round({'cum_cterr': 0,
                                                             'cterr': 0,
                                                             'mean_error': 3,
                                                             'frequency_square_error': 3,
                                                             'model_predictions': 3})
            self.holdout_frequencies = holdout_frequencies

        return self.frequency_stats, self.monetary_stats, self.holdout_frequencies
