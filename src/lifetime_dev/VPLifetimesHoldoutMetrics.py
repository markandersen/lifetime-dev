import numpy as np


class VPLifetimesHoldoutMetrics:
    """
    VPLifetimesHoldoutMetrics calculates key metrics for a holdout set in comparison
    with a prediction set

    Format:
    ground_truth:
      id
      frequency_holdout
      monetary_value_holdout

    prediction:
      id
      predict_order_count
      predict_spend_per_order
      predict_total_spend

    Metrics are done on the basis of those predicted.
    So customers might not be predicted.  And warnings would need to be issued in some cases.
    """

    def __init__(self,
                 prediction_df,
                 ground_truth_df):
        #
        # Warning: Be sure that frequency in holdout period includes *first* order in holdout period.
        self.compare = prediction_df.merge(ground_truth_df, how='inner', on='id')
        assert(len(self.compare) == len(prediction_df)), \
            f'Ground missing for some ids comparison len {len(self.compare)} and prediction {len(prediction_df)}'

        # intermediate variable computation for order count
        self.compare['order_count_error'] = self.compare.predict_order_count - self.compare.frequency_holdout
        self.compare['order_count_abserror'] = self.compare.order_count_error.abs()
        self.compare['order_count_sqerror'] = self.compare.order_count_error.pow(2)

        # intermediate variable computation for monetary value (residual LTV)
        self.compare['mval_error'] = self.compare.predict_total_spend - self.compare.monetary_value_holdout
        self.compare['mval_abserror'] = self.compare.mval_error.abs()
        self.compare['mval_sqerror'] = self.compare.mval_error.pow(2)

        # intermediate variable computation for conditional expected expenditure
        # gamma-gamma: can evaluate spend conditional on ordering
        # we only look for those who have purchased
        purchasers = self.compare[self.compare.frequency_holdout > 0]
        # we might have some with absolute zero predict purchases and we cannot see conditional exp money
        # hopefully none occur in practice
        len_before_screening = len(purchasers)
        purchasers = purchasers[purchasers.predict_order_count > 0]
        len_after_screening = len(purchasers)
        if len_before_screening > len_after_screening:
            print(f'Warning: Some zero frequency predictions removed, reducing set from '
                  f'{len_before_screening} to {len_after_screening}')
        purchasers['holdout_mon_avg'] = purchasers.monetary_value_holdout.div(purchasers.frequency_holdout)
        purchasers['predict_mon_avg'] = purchasers.predict_total_spend.div(purchasers.predict_order_count)
        purchasers['mon_avg_error'] = purchasers.holdout_mon_avg - purchasers.predict_mon_avg
        purchasers['mon_avg_abserror'] = purchasers.mon_avg_error.abs()
        purchasers['mon_avg_sqerror'] = purchasers.mon_avg_error.pow(2)

        # computed metrics
        self.base_metrics = {
            'numorder_mae': self.compare.order_count_abserror.mean(),
            # 'numorder_mse': self.compare.order_count_sqerror.mean(),
            'numorder_rmse': np.sqrt(self.compare.order_count_sqerror.mean()),
            'numorder_predicted_orders': self.compare.predict_order_count.sum(),
            'numorder_actual_orders': self.compare.frequency_holdout.sum(),
            'numorder_predicted_per_customer': self.compare.predict_order_count.mean(),
            'numorder_actual_per_customer': self.compare.frequency_holdout.mean(),
            'numorder_rspearman': self.compare[['predict_order_count',
                                                'frequency_holdout']].corr(method='spearman').loc['predict_order_count',
                                                                                                  'frequency_holdout'],
            'mval_mae': self.compare.mval_abserror.mean(),
            # 'mval_mse': self.compare.mval_sqerror.mean(),
            'mval_rmse': np.sqrt(self.compare.mval_sqerror.mean()),
            'mval_predicted': self.compare.predict_total_spend.sum(),
            'mval_actual': self.compare.monetary_value_holdout.sum(),
            'mval_predicted_per_customer': self.compare.predict_total_spend.mean(),
            'mval_actual_per_customer': self.compare.monetary_value_holdout.mean(),
            'mval_rspearman':  self.compare[['predict_total_spend',
                                                'monetary_value_holdout']].corr(method='spearman').loc['predict_total_spend',
                                                                                                  'monetary_value_holdout'],

            'cond_mon_mae': purchasers.mon_avg_abserror.mean(),
            # 'cond_mon_mse': purchasers.mon_avg_sqerror.mean(),
            'cond_mon_rmse': np.sqrt(purchasers.mon_avg_sqerror.mean()),
            'cond_mon_predicted_mean': purchasers.predict_total_spend.mean(),
            'cond_mon_actual_mean': purchasers.monetary_value_holdout.mean(),
            'cond_mon_predicted_per_customer': purchasers.predict_mon_avg.mean(),
            'cond_mon_actual_per_customer': purchasers.holdout_mon_avg.mean(),
            'cond_mon_rspearman': purchasers[['predict_mon_avg',
                                              'holdout_mon_avg']].corr(method='spearman').loc['predict_mon_avg',
                                                                                              'holdout_mon_avg'],

        }

    @staticmethod
    def get_gamma_gamma_expected_spend_metrics(purchasers, prediction_column, gt_column):
        """
        computes gamma-gamma metrics for expected spend
        only records where purchases have occurred should be included

        purchases: dataFrame of data
        prediction_column: column with predicted expected spend
        gt_column: column with actual spend

        """
        purchasers['mon_avg_error'] = purchasers[gt_column] - purchasers[prediction_column]
        purchasers['mon_avg_abserror'] = purchasers.mon_avg_error.abs()
        purchasers['mon_avg_sqerror'] = purchasers.mon_avg_error.pow(2)

        return {
            'cond_mon_mae': purchasers.mon_avg_abserror.mean(),
            'cond_mon_rmse': np.sqrt(purchasers.mon_avg_sqerror.mean()),
            'cond_mon_predicted_mean': purchasers.predict_total_spend.mean(),
            'cond_mon_actual_mean': purchasers.monetary_value_holdout.mean(),
            'cond_mon_predicted_per_customer': purchasers.predict_mon_avg.mean(),
            'cond_mon_actual_per_customer': purchasers.holdout_mon_avg.mean(),
            'cond_mon_rspearman': purchasers[[prediction_column,
                                              gt_column]].corr(method='spearman').loc[prediction_column, gt_column]
        }
