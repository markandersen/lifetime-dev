import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.ticker as mtick


class TimeToOrderPlots:

    #
    # Plots for Time to Nth Order
    #

    @staticmethod
    def plt_days_to_order_charts(order_df: pd.DataFrame,
                                 suptitle_text: str,
                                 filename: str = None,
                                 figsize: (int, int) = (12, 16),
                                 suptitle_size: int = 18,
                                 annotate_size: int = 9,
                                 label_size: int = 10,
                                 subtitle_size: int = 11):
        """

        :param order_df: DataFrame with information on shoppers and distinct order dates
        :param suptitle_text: text to use for title of chart and filename
        :param filename: if provided, figure is saved to this full path name
        :param figsize: figure size of plot generated
        :param suptitle_size: font size of titles on super title
        :param annotate_size: font size of annotations on charts
        :param label_size: font size of x and y labels
        :param subtitle_size: font size of titles on subplots
        """

        #
        # collapse orders to dates, so we have an entry for each shopper/date combination
        order_dates = order_df.groupby(['SHOPPER_ID', 'ORDER_DATE_DT'])['AGP_BUDGET_USD'].sum().reset_index()

        # create DF showing each shopper and how may distinct order dates they have, called ORDER_COUNT
        per_shopper = order_dates.groupby('SHOPPER_ID').ORDER_DATE_DT.nunique().reset_index()
        per_shopper.rename(columns={'ORDER_DATE_DT': 'ORDER_COUNT'}, inplace=True)

        # find shopper entries which have 2 or more orders
        multi_shopper = per_shopper[per_shopper.ORDER_COUNT >= 2]
        multi_shopper_orders = multi_shopper[['SHOPPER_ID']].merge(order_dates, how='inner', on='SHOPPER_ID')
        multi_shopper_orders['ORDER_RANK'] = multi_shopper_orders.groupby('SHOPPER_ID').ORDER_DATE_DT.rank()

        # DF with the first orders only
        first_orders = multi_shopper_orders[multi_shopper_orders.ORDER_RANK == 1]
        # DF with the second orders only
        second_orders = multi_shopper_orders[multi_shopper_orders.ORDER_RANK == 2]
        # DF with relationship between first order and second order.  Suffixes help distinguish which
        two_one_orders = second_orders.merge(first_orders, on='SHOPPER_ID', suffixes=('_2', '_1'))
        two_one_orders['DATE_DIFF'] = (two_one_orders.ORDER_DATE_DT_2 - two_one_orders.ORDER_DATE_DT_1).dt.days
        two_one_orders['DATE_DIFF_YEAR'] = two_one_orders.DATE_DIFF.div(365.0)
        assert two_one_orders.DATE_DIFF.min() >= 1, 'Should not have any 0 or negative DATE_DIFF'

        # third order
        third_orders = multi_shopper_orders[multi_shopper_orders.ORDER_RANK == 3]
        three_two_one_orders = third_orders.merge(two_one_orders, on='SHOPPER_ID')
        three_two_one_orders.rename(columns={'DATE_DIFF': 'DATE_DIFF_1_2',
                                             'ORDER_DATE_DT': 'ORDER_DATE_DT_3',
                                             'TOTAL_BOOKINGS_BUDGET_USD': 'TOTAL_BOOKINGS_BUDGET_USD_3'}, inplace=True)
        three_two_one_orders['DATE_DIFF_2_3'] = (
                    three_two_one_orders.ORDER_DATE_DT_3 - three_two_one_orders.ORDER_DATE_DT_2).dt.days
        three_two_one_orders['DATE_DIFF_1_3'] = (
                    three_two_one_orders.ORDER_DATE_DT_3 - three_two_one_orders.ORDER_DATE_DT_1).dt.days

        third_order = three_two_one_orders.sort_values('DATE_DIFF_1_3')
        third_order['ONE'] = 1
        third_order['DATE_DIFF_CUM'] = (third_order.ONE.cumsum()) / len(third_order)

        # Plot Configuration
        fig, axes = plt.subplots(4, 2, figsize=figsize)
        fig.subplots_adjust(top=0.95)

        #
        # Plotting
        #
        # -- Number of Orders per Shopper
        max_order_count = 10
        ax = axes[0, 0]
        per_shopper[per_shopper.ORDER_COUNT < max_order_count].ORDER_COUNT.hist(bins=range(max_order_count),
                                                                                density=True,
                                                                                ax=ax)
        ax.set_xticks(range(1, max_order_count))
        ax.set_xlim(1, max_order_count)
        ax.set_xlabel('Order Dates Per Shopper', fontsize=label_size)
        ax.set_title(f'Order Dates Per Shopper', fontsize=subtitle_size)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
        ax.set_ylabel('Percent of Shoppers', fontsize=label_size)
        ymin, ymax = ax.get_ylim()
        ax.set_yticks(np.arange(0, ymax, 0.05))

        # -- Years to Second Order Date
        ax = axes[0, 1]
        max_years = 5
        two_one_orders[two_one_orders.DATE_DIFF_YEAR < max_years].DATE_DIFF_YEAR.hist(bins=100, ax=ax)
        ax.set_xticks(np.arange(0, max_years + 1))
        ax.get_yaxis().set_major_formatter(mtick.FuncFormatter(lambda x, p: format(int(x), ',')))
        ax.set_title(f'Years to Second Order Date', fontsize=subtitle_size)
        ax.set_ylabel('Shopper Count', fontsize=label_size)
        ax.set_xlabel('Years', fontsize=label_size)

        # -- Cumulative Days to Second Order Date
        # above code does not expose the values out for my own use
        ax = axes[1, 0]
        second_order = two_one_orders.sort_values('DATE_DIFF')
        second_order['ONE'] = 1
        second_order['DATE_DIFF_CUM'] = (second_order.ONE.cumsum()) / len(second_order)
        max_years = 5
        second_order.plot(x='DATE_DIFF', y='DATE_DIFF_CUM', grid=True, legend='', ax=ax)
        ax.set_xticks(np.arange(0, 365 * max_years, 365))
        ax.set_xlabel('Days Between First and Second Order Date', fontsize=label_size)
        for threshold in [0.5, 0.7, 0.8, 0.90, 0.95, 0.99]:
            date_diff = second_order[second_order.DATE_DIFF_CUM > threshold].DATE_DIFF.min()
            # plt.axvline(x=ddiff, linestyle='-.', color='xkcd:orange')
            line = Line2D(xdata=[0, date_diff], ydata=[threshold, threshold], color='xkcd:orange', linestyle='-.')
            ax.add_line(line)
            line = Line2D(xdata=[date_diff, date_diff], ydata=[0, threshold], color='xkcd:orange', linestyle='-.')
            ax.add_line(line)
            ax.annotate(f'{"{:0.0f}".format(np.round(threshold * 100, 2))}%: {date_diff} days', xy=(0, threshold + .01))

        ax.annotate(text='Note annual inflections', xy=(1000, .01), fontsize=annotate_size)
        ax.set_xlim(0, 365 * max_years)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
        ax.set_ylabel('Percent of Shoppers', fontsize=label_size)
        ax.set_xlabel('Days to Second Order Date', fontsize=label_size)
        ax.set_title(f'Days to Second Order Date', fontsize=subtitle_size)

        # -- Days first to second order date
        ax = axes[1, 1]
        third_order.plot(x='DATE_DIFF_1_3', y='DATE_DIFF_CUM', grid=True, legend='', ax=ax)
        max_years = 5
        ax.set_xticks(np.arange(0, 365 * max_years, 365))
        ax.set_xlabel('Days Between First and Second Order Date', fontsize=label_size)
        for threshold in [0.5, 0.7, 0.8, 0.90, 0.95, 0.99]:
            date_diff = third_order[third_order.DATE_DIFF_CUM > threshold].DATE_DIFF_1_3.min()
            # plt.axvline(x=ddiff, linestyle='-.', color='xkcd:orange')
            line = Line2D(xdata=[0, date_diff], ydata=[threshold, threshold], color='xkcd:orange', linestyle='-.')
            ax.add_line(line)
            line = Line2D(xdata=[date_diff, date_diff], ydata=[0, threshold], color='xkcd:orange', linestyle='-.')
            ax.add_line(line)
            ax.annotate(f'{"{:0.0f}".format(np.round(threshold * 100, 2))}%: {date_diff} days', xy=(0, threshold + .01))
        ax.set_xlim(0, 365 * max_years)
        ax.annotate(text='Note annual inflections', xy=(1000, .01), fontsize=annotate_size)
        ax.set_title(f'Days from First to Third Order Date', fontsize=subtitle_size)
        ax.set_xlabel(f'Days', fontsize=label_size)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
        ax.set_ylabel('Percent of Shoppers', fontsize=label_size)

        # -- Days first to third order v. days first to second order
        ax = axes[2, 0]
        thresh = 380 * 4
        cap = three_two_one_orders[
            (three_two_one_orders.DATE_DIFF_2_3 < thresh) & (three_two_one_orders.DATE_DIFF_1_2 < thresh)]
        cap = cap[cap.DATE_DIFF_1_2 + cap.DATE_DIFF_2_3 < thresh]
        cap.plot.scatter(x='DATE_DIFF_1_2', y='DATE_DIFF_2_3', s=.01, ax=ax)
        ln = Line2D(xdata=[0, thresh], ydata=[thresh, 0], color='xkcd:black', linestyle='solid')
        ax.add_line(ln)
        ax.get_yaxis().set_major_formatter(mtick.FuncFormatter(lambda x, p: format(int(x), ',')))
        ax.set_ylabel('Days Between Second and Third Order', fontsize=label_size)
        ax.set_xlabel('Days Between First and Second Order', fontsize=label_size)
        ax.set_title(f'Time to Second Order Date v. Third Order Date', fontsize=subtitle_size)

        #
        # -- First and second customer order days when between 300-400
        ax = axes[3, 0]
        second_order[(second_order.DATE_DIFF > 300) & (second_order.DATE_DIFF < 400)].DATE_DIFF.hist(bins=50, ax=ax)
        ax.get_yaxis().set_major_formatter(mtick.FuncFormatter(lambda x, p: format(int(x), ',')))
        ax.set_ylabel('# Shoppers', fontsize=label_size)
        ax.set_xlabel('# Days', fontsize=label_size)
        ax.set_title(f'Days Between First and Second Customer Order', fontsize=subtitle_size)

        #
        # -- Days between 335-395 days
        # Extra criteria that this chart never considers before holiday 2013 to let it spread out graphically
        ax = axes[3, 1]
        annual_repeat = second_order[(second_order.DATE_DIFF >= 335) & (second_order.DATE_DIFF < 395)]
        annual_repeat = annual_repeat[annual_repeat.ORDER_DATE_DT_1 > pd.to_datetime('2013-07-01', format='%Y-%m-%d')]
        annual_repeat.ORDER_DATE_DT_1.hist(bins=90, ax=ax)  # approximately one bin per month
        ax.set_xlabel('First Order Date', fontsize=label_size)
        ax.get_yaxis().set_major_formatter(mtick.FuncFormatter(lambda x, p: format(int(x), ',')))
        ax.set_ylabel('# Shoppers', fontsize=label_size)
        ax.set_title(f'First Order Dates for Customers With 335-395 Days to Second Order', fontsize=subtitle_size)
        plt.suptitle(suptitle_text, fontsize=suptitle_size, y=0.98)
        plt.tight_layout(rect=(0, 0, 1, 0.96))
        if filename is not None:
            plt.savefig(filename)
        plt.show()

    @staticmethod
    def plt_days_to_nth_order(order_df: pd.DataFrame,
                              target_order_count: int,
                              ax: plt.Axes = None,
                              label_size: int = 10,
                              title_size: int = 11):
        """
        Make subplot showing time to 2nd order for shoppers with N order (date)s

        order_df: DataFrame with the order dates (might not be fully collapsed due to group by)
        target_order_count: N which is the minimum number of order dates for shoppers being studied
        ax: where to draw plot, if provided,
        label_size: size of x and y labels
        title_size: size of the plot title
        """

        #
        # collapse orders to dates, so we have an entry for each shopper/date combination
        order_dates = order_df.groupby(['SHOPPER_ID', 'ORDER_DATE_DT'])['AGP_BUDGET_USD'].sum().reset_index()

        # create DF showing each shopper and how may distinct order dates they have, called ORDER_COUNT
        per_shopper = order_dates.groupby('SHOPPER_ID').ORDER_DATE_DT.nunique().reset_index()
        per_shopper.rename(columns={'ORDER_DATE_DT': 'ORDER_COUNT'}, inplace=True)

        # find shopper entries which have order_target or more orders
        target_shopper = per_shopper[per_shopper.ORDER_COUNT >= target_order_count]
        target_shopper_orders = target_shopper[['SHOPPER_ID']].merge(order_dates, how='inner', on='SHOPPER_ID')
        target_shopper_orders['ORDER_RANK'] = target_shopper_orders.groupby('SHOPPER_ID').ORDER_DATE_DT.rank()

        # DF with the first orders only
        first_orders = target_shopper_orders[target_shopper_orders.ORDER_RANK == 1]
        # DF with the second orders only
        second_orders = target_shopper_orders[target_shopper_orders.ORDER_RANK == 2]

        # DF with relationship between first order and second order.  Suffixes help distinguish which
        two_one_orders = second_orders.merge(first_orders, on='SHOPPER_ID', suffixes=('_2', '_1'))
        two_one_orders['DATE_DIFF'] = (two_one_orders.ORDER_DATE_DT_2 - two_one_orders.ORDER_DATE_DT_1).dt.days
        two_one_orders['DATE_DIFF_YEAR'] = two_one_orders.DATE_DIFF.div(365.0)
        assert two_one_orders.DATE_DIFF.min() >= 1, 'Should not have any 0 or negative DATE_DIFF'

        second_order = two_one_orders.sort_values('DATE_DIFF')
        second_order['ONE'] = 1
        second_order['DATE_DIFF_CUM'] = (second_order.ONE.cumsum()) / len(second_order)
        max_years = 5
        second_order.plot(x='DATE_DIFF', y='DATE_DIFF_CUM', grid=True, legend='', ax=ax)
        ax.set_xticks(np.arange(0, 365 * max_years, 365))
        ax.set_xlabel('Days Between First and Second Order Date', fontsize=label_size)
        for threshold in [0.5, 0.7, 0.8, 0.90, 0.95, 0.99]:
            date_diff = second_order[second_order.DATE_DIFF_CUM > threshold].DATE_DIFF.min()
            # plt.axvline(x=ddiff, linestyle='-.', color='xkcd:orange')
            line = Line2D(xdata=[0, date_diff], ydata=[threshold, threshold], color='xkcd:orange', linestyle='-.')
            ax.add_line(line)
            line = Line2D(xdata=[date_diff, date_diff], ydata=[0, threshold], color='xkcd:orange', linestyle='-.')
            ax.add_line(line)
            ax.annotate(f'{"{:0.0f}".format(np.round(threshold * 100, 2))}%: {date_diff} days', xy=(0, threshold + .01))

        ax.set_xlim(0, 365 * max_years)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
        ax.set_ylabel(f'Percent of {target_order_count} Order Shoppers', fontsize=label_size)
        ax.set_xlabel('Days to Second Order Date', fontsize=label_size)
        ax.set_title(f'{target_order_count} Order Shoppers: Days to Second Order Date', fontsize=title_size)
