from lifetimes.datasets import load_cdnow_summary_data_with_monetary_value
from lifetimes import GammaGammaFitter

#
# Code shared with Lifetimes github illustrating negative community values (q<1) in practice
#

summary_with_money_value = load_cdnow_summary_data_with_monetary_value()
returning_customers_summary = summary_with_money_value[summary_with_money_value['frequency']>0]

for penalizer_coef in [0, 0.1, 0.5]:
    ggf = GammaGammaFitter(penalizer_coef=penalizer_coef)
    ggf.fit(returning_customers_summary['frequency'], returning_customers_summary['monetary_value'])
    print(f'{penalizer_coef}: {ggf.params_}')
