from lifetimes.datasets import load_transaction_data
from lifetimes.utils import calibration_and_holdout_data

#
# This code is to prove the care which must be taken in the use of the term
# frequency in Lifetimes.
#
# In calibration_and_holdout_data:
#  The calibration period has one subtracted from the transactions so
#  frequency = number of repeat transactions (per the docs)
#
# In the holdout period, the frequency does not have one subtracted from
# within the period, but it is assumed that *all* transactions are repeat
# transactions.
#
# Therefore, different results occur when one calls the calibration_and_holdout
# than if one separately called summary_from_transaction_data for the holdout period.
#
trans = load_transaction_data()
print(trans.date.max())

# below we merely take one day off the data set so we can isolate an
# easily tested subset
summary_cal_holdout = calibration_and_holdout_data(trans,
                                                   'id',
                                                   'date',
                                                   calibration_period_end='2014-12-30',
                                                   observation_period_end='2014-12-31')

#
# these shoppers have frequency_holdout > 0 because they shopped in the last day
freq_1_shoppers = summary_cal_holdout[summary_cal_holdout.frequency_holdout > 0]
print(freq_1_shoppers)

# these are the transactions for the last day shoppers
last_day_trans = trans[trans.date=='2014-12-31 00:00:00']
print(last_day_trans)

# Looking at shopper 2476 shows that there is one transaction in the calibration
# period not contributing to frequency, but this subtraction does not occur
# in the holdout period.


