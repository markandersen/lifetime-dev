from tqdm import tqdm
import pandas as pd
from lifetimes.datasets import load_cdnow_summary_data_with_monetary_value
from lifetimes import GammaGammaFitter, ModifiedBetaGeoFitter
import lifetimes
pd.options.display.max_columns = 40
print(f'lifetimes version: {lifetimes.__version__}')

#
# Test Lifetimes quickstart LTV scoring
#
# Finding: fractional months not handled correctly due to underlying type assumption of int (API says float)
#

subset = load_cdnow_summary_data_with_monetary_value()

#
# Manually fit the models to get parameters and results
#
forecast_months = [1, 1.5, 2, 3, 36, 72]
# positive bookings required, and bookings must be float64
subset_summary = subset[subset.monetary_value > 0]
subset_summary['prior_value'] = subset_summary.monetary_value * (subset_summary.frequency + 1)
mgbf = ModifiedBetaGeoFitter(penalizer_coef=0.0)
mgbf.fit(subset_summary.frequency,
         subset_summary.recency,
         subset_summary['T'])
print(mgbf.summary)

ggm = GammaGammaFitter(penalizer_coef=0.0)
subset_ggm_fit = subset_summary[subset_summary.frequency > 0]
ggm.fit(subset_ggm_fit.frequency, subset_ggm_fit.monetary_value)
print(ggm.summary)

#
# score with the fit models
#
subset_summary['prob_alive'] = mgbf.conditional_probability_alive(subset_summary.frequency,
                                                                  subset_summary.monetary_value,
                                                                  subset_summary['T'])

subset_summary['conditional_expected_value_per_order'] = \
    ggm.conditional_expected_average_profit(
        subset_summary.frequency,
        subset_summary.monetary_value
    )
for forecast_months in tqdm(forecast_months):
    forecast_days = forecast_months * 30
    subset_summary[f'fcst_orders_{forecast_days}'] = \
        mgbf.conditional_expected_number_of_purchases_up_to_time(
            forecast_days,
            # later optimization maybe can do in one pass.
            # I think this is in units of the data not months
            subset_summary.frequency,
            subset_summary.recency,
            subset_summary['T']
        )
    subset_summary[f'fcst_value_{forecast_days}'] = \
        ggm.customer_lifetime_value(
            mgbf,
            subset_summary.frequency,
            subset_summary.recency,
            subset_summary['T'],
            subset_summary.monetary_value,
            time=forecast_months,  # annoying this must be in months not units of data
            discount_rate=0.0,  # discount removed for comparability to manual calculation and due to short time period
            freq='D'
        )

    # manual calculation (with no discounting applied)
    subset_summary[f'fcst_inc_value_{forecast_days}'] = \
        subset_summary[f'fcst_orders_{forecast_days}'] * subset_summary['conditional_expected_value_per_order']


print('Notice that fcst_value_45 is the same as fct_value_60')
print(subset_summary[:2])
