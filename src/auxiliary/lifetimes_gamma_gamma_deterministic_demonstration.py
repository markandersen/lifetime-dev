import os
import logging
import pandas as pd
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis

#
# - Test Proves that Gamma-Gamma Fit Returns Consistent Results Across Runs
#
# Concern was whether gamma-gamma fit was stochastic
#
# While this could be a test, it is really a test of Lifetimes and a point of interest.
# It does not belong in a battery of unit tests.
#

#
# - General Processing
#
log_dir = os.path.join('..', '..', 'logs', 'temp')
log_filename = os.path.join(log_dir, 'test_hyperparamter_variation.log')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
if os.path.exists(log_filename):
    os.remove(log_filename)
logging.basicConfig(filename=log_filename,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger('__name__')


def fit_order_data(orders_df: pd.DataFrame,
                   observation_date: str,
                   bg_fit: bool=True,
                   bg_penalizer: float=0.01,
                   gg_fit: bool=True,
                   gg_penalizer: float=0.0):
    """
    fits order data with MBG and GGF for the dataset and returns the parameters resulting

    Intended for caller to fit the dataset and then record or analyze those parameters

    :param orders_df: dataset which is to be fit.  (Caller can sample, filter, etc.)
    :param observation_date: observation date to limit observations in passed data set
    :param bg_fit: bool whether to fit the bg model
    :param bg_penalizer: penalizer to use with bg model
    :param gg_fit: bool whether to fit the gg model
    :param gg_penalizer: penalizer to use with the gg model
    :return: resulting parameters
    """
    vp_lifetime_data = VPLifetimesData(orders_df)
    vp_lifetime_analysis = VPLifetimesAnalysis(vp_lifetime_data)

    summary_data = vp_lifetime_data.get_summary_data_from_transaction(observation_date)
    summary_data = summary_data[summary_data.frequency > 0]
    vp_lifetime_analysis.reset_order_model(penalizer=bg_penalizer)

    fit_parameters = {}
    if bg_fit:
        vp_lifetime_analysis.order_model.fit(summary_data.frequency,
                                             summary_data.recency,
                                             summary_data['T'])
        bgf_expect_params = ['r', 'alpha', 'a', 'b']
        for param in bgf_expect_params:
            fit_parameters[f'b-{param}'] = vp_lifetime_analysis.order_model.params_[param]
    if gg_fit:
        ggf_model = vp_lifetime_analysis.gamma_gamma_fit(summary_data.frequency,
                                                         summary_data.monetary_value,
                                                         penalizer=gg_penalizer)
        ggf_expect_params = ['p', 'q', 'v']
        for param in ggf_expect_params:
            fit_parameters[f'g-{param}'] = ggf_model.params_[param]
    return fit_parameters


src = os.path.join('..', '..', 'data', '01_raw', 'combined-data.pkl')
orders_df = pd.read_pickle(src)
model_results = []
# shorter start date used to make this test run more quickly
start_date = '2019-07-01'
end_date = '2020-07-01'
# arbitrary penalizer used for this test
gg_penalizer = 0.05
for i in tqdm(range(5)):
    for country in ['IE']:
        cohort = f'{country}-GGP:{gg_penalizer}'
        analyze_orders_df = orders_df[orders_df.ACCOUNT_CREATION_COUNTRY == country]
        analyze_orders_df = analyze_orders_df[analyze_orders_df.ORDER_DATETIME > start_date]
        analyze_orders_df = analyze_orders_df[analyze_orders_df.FIRST_ORDER_DATETIME > start_date]
        results = fit_order_data(analyze_orders_df,
                                 end_date,
                                 bg_fit=False,
                                 gg_fit=True,
                                 gg_penalizer=gg_penalizer)
        results['country'] = country
        results['cohort'] = cohort
        results['start_date'] = start_date
        results['end_date'] = end_date
        results['seasonal'] = ''
        results['sample_percent'] = 100
        model_results.append(results)

results_df = pd.DataFrame.from_dict(model_results)

# de-dupe and test length == 1
results_df.drop_duplicates(inplace=True)
assert len(results_df) == 1, 'Length of results should be 1 because deterministic gamma-gamma runs return same results.'
