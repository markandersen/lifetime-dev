import os
from time import strptime
from datetime import date
import pandas as pd



#
# - Data
#

src = os.path.join('..', '..', 'data', '01_raw',
                   'VPLifetimes--orders--IE-ES--2010-07-01-2020-07-21--2010-07-01-2020-07-21.0.11.pkl')
all_orders_df = pd.read_pickle(src)
all_orders_df.loc[all_orders_df.AGP_BUDGET_USD <= 0, 'AGP_BUDGET_USD'] = 0.01
all_orders_df.AGP_BUDGET_USD = all_orders_df.AGP_BUDGET_USD.astype('float64')


datasets = {}
calibration_end = '2019-06-30'
observation_end = '2020-07-01'
calibration_end_dt = pd.to_datetime(calibration_end, format="%Y-%m-%d").date()
observation_end_dt = pd.to_datetime(observation_end, format="%Y-%m-%d").date()
holdout_duration_days = (observation_end_dt - calibration_end_dt).days
print(f'Testing with calibration end {calibration_end} and observation end {observation_end}')
print(f'Holdout duration {holdout_duration_days}')
for country in ['IE', 'ES']:  # quicker country first in case we do break out later
    orders_df = all_orders_df[all_orders_df.ACCOUNT_CREATION_COUNTRY == country]
    print(f'{country}: {len(orders_df)}')
