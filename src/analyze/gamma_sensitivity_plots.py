import os
import pandas as pd
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesHoldoutPlots import VPLifetimesHoldoutPlots
pd.options.display.max_columns = 50
#
# Sample data
#
datasets = [
#    (None, 'IE - Differing Calibration Dates', 'test_IE_2020-07-17-1.csv'),
#    (None, 'ES - Differing 70% Random Samples', 'test_ES-2020-07-17-1.csv')
#    (None, 'test hyperparameters', 'test_hyperparameters_2015-2020.csv')
    ('repetitive', 'GammaGamma Regularization Plot', 'test_hyperparameters_2010-2020.csv')
]

for set_type, cohort_descriptor, filename in tqdm(datasets):
    df = pd.read_csv(os.path.join('..', '..', 'data', '09_test_data', filename))


    if set_type == 'repetitive':
        # specific problems with data written
        df.cohort = df.cohort.str.replace('1e-06', '0.000001')
        df.cohort = df.cohort.str.replace('1e-05', '0.00001')
        # Get gamma-cohort from the listed cohorts
        df[['country_split', 'beta_cohort', 'gamma_cohort']] = df.cohort.str.split('-', expand=True)
        df.cohort = df.gamma_cohort

    # if cohort not filled in then end_date is better
    df.loc[df.cohort == '', 'cohort'] = df.end_date

    # repetitive means that the columns are not unique
    # safe operation for all sets to eliminate repeat rows
    temp_df = df[['country', 'cohort', 'g-p', 'g-q', 'g-v']].drop_duplicates()
    df = temp_df

    country_list = df.country.unique().tolist()
    for country in country_list:
        country_df = df[df.country == country]
        plot_title = f'{country} - {cohort_descriptor}'
        #
        # Tests
        #
        plot_filename = os.path.join('..', '..', 'data', '08_reporting',
                                     f'gamma-sensitivity-{plot_title}.png'.replace(' ', '_'))
        VPLifetimesHoldoutPlots.plot_gamma_gamma_sensitivity_sets(country_df,
                                                                  'cohort',
                                                                  suptitle_prefix=f'{country}: {cohort_descriptor}',
                                                                  save_filename=plot_filename,
                                                                  preferred_gamma_label='0')

        parameter_df = country_df[['cohort', 'g-p', 'g-q', 'g-v']]
        plot_filename = f'gamma-stability-{plot_title}.png'.replace(' ', '_')
        VPLifetimesHoldoutPlots.plot_parameter_stability_pairplot(parameter_df,
                                                                  'cohort',
                                                                  plot_title,
                                                                  save_filename=plot_filename)

#
# Additional plots
#
#
# make facet grid for results
#


#sns.set(style='white')
#g = sns.PairGrid(df[['p', 'q']], diag_sharey=False)
#g.map_upper(sns.scatterplot)
#g.map_lower(sns.kdeplot, colors='C0')
#g.map_diag(sns.kdeplot, lw=2)
#plt.show()


# scatterplot for 1x1
# lets turn v into size and see how that goes
#g = sns.scatterplot(data=df, x='p', y='q', legend='full',
#                    # size='v',
#                    hue='cohort')
#plt.show()


# relplot with extra categorical variables




