import os
import pandas as pd
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesHoldoutPlots import VPLifetimesHoldoutPlots
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis
from src.lifetime_dev.VPLifetimesOrderPredictAnalyze import VPLifetimesOrderPredictAnalyze
from src.lifetime_dev.VPLifetimesHoldoutMetrics import VPLifetimesHoldoutMetrics
from pathlib import Path
from kedro.framework.context import load_context
pd.options.display.max_columns = 50

#
# This file is used for getting the calibration and holdout scores
# which are reported for RMSE, MAE and RSpearman.
#
# It's a bit awkward to grab the numbers right now as these are written into some
# dictionaries and printed out to console from where they can be rounded & grabbed for use.
#
#
countries = ['IE', 'ES']
start_date = '2010-07-01'
end_date = '2020-08-01'
project_path = Path(os.getcwd()).parents[1].resolve()  # 2 levels up from this file
context = load_context(project_path)
orders_df = context.catalog.load("ie_es_2010_2020")
orders_df.AGP_BUDGET_USD = orders_df.AGP_BUDGET_USD.astype('float64')
orders_df['ORDER_DATE_DT'] = pd.to_datetime(orders_df.ORDER_DATE, format='%Y-%m-%d')

country_field = 'ACCOUNT_CREATION_COUNTRY'
observe_precovid_date = '2020-02-28'
order_date_field = 'ORDER_DATE'
param_fields = ['b-r', 'b-alpha', 'b-a', 'b-b', 'g-p', 'g-q', 'g-v']
fit_results = {}
rows = []
freq_multiplier = 1
calibration_period_end = '2019-09-30'

model_types = ['ModifiedBetaGeo']  # ['BetaGeo', 'ModifiedBetaGeo']
countries = ['IE', 'ES']
end_dates = [end_date] # [end_date, observe_precovid_date]
freq = 'D'  # 'W' not supported in any way


for country in tqdm(countries):
    country_df = orders_df[orders_df[country_field] == country]

    # one country for now
    country_results = {}
    for obs_date in tqdm(end_dates):
        #
        # Consider including post-COVID response and not
        #
        freq_results = {}
        for model_type in model_types:
            # Compare daily and weekly
            descriptor = f'{country}-cal:{calibration_period_end}-obs thru:{obs_date} ModelType:{model_type} freq:{freq}'
            print(f'Scores for {descriptor}')
            life_data = VPLifetimesData(country_df)

            #
            # Copied manner of doing this I had before
            # can refine
            #
            vp_lifetime_analysis = VPLifetimesAnalysis(life_data)
            vplop = VPLifetimesOrderPredictAnalyze(life_data,
                                                   calibration_period_end=calibration_period_end,
                                                   observation_period_end=obs_date)
            data = vplop.calibration(vp_lifetime_analysis, model_type, 0.0)
            _ = vplop.calc_order_predictions(vp_lifetime_analysis)

            if True:
                #
                # extra processing time to make graphs
                #
                plots = VPLifetimesHoldoutPlots(vplop.calholddata)

                file_base_name = f'{country}-calend-{calibration_period_end}-obs-{obs_date}-model-{model_type}'

                # uncomment later if needed
                # plots.plot_default_lifetimes_charts_prob_alive(vp_lifetime_analysis.order_model, descriptor)

                # two custom charts
                plots.plot_holdout_frequency_charts(suptitle=f'Frequency holdouts: {descriptor}',
                                                    save_image_filename=os.path.join('..', '..', 'data', '08_reporting',
                                                                                     f'{file_base_name}-frequency.png'))
                plots.plot_model_rank_vs_holdout(suptitle=f'Rank v. holdout: {descriptor}',
                                                 save_image_filename=os.path.join('..', '..', 'data', '08_reporting',
                                                                                  f'{file_base_name}-rank.png'))

            #
            # Use new code to get scores
            #
            temp = vplop.calholddata.copy()
            temp = temp.reset_index()
            temp.rename(columns={'model_prediction_bookings': 'predict_total_spend',
                                 'model_predictions': 'predict_order_count',
                                 'SHOPPER_ID': 'id'}, inplace=True)

            # disregard predict_spend_per_order -- not used in any metrics
            # but it is expected in the future.

            # pare down the columns to avoid conflict based renames on merge
            compared = VPLifetimesHoldoutMetrics(temp[['id', 'frequency_holdout', 'monetary_value_holdout']],
                                                 temp[['id', 'predict_order_count', 'predict_total_spend']])

            print(f'{descriptor} scores: {compared.base_metrics}')

            temp.predict_order_count = 0
            temp.predict_total_spend = 0
            zcompared = VPLifetimesHoldoutMetrics(temp[['id', 'frequency_holdout', 'monetary_value_holdout']],
                                                 temp[['id', 'predict_order_count', 'predict_total_spend']])

            print(f'{descriptor} zero guess scores: {zcompared.base_metrics}')
