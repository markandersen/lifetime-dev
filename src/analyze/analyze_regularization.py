import os
from tqdm import tqdm
from datetime import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from lifetimes.utils import summary_data_from_transaction_data
from lifetimes import GammaGammaFitter, ParetoNBDFitter, ModifiedBetaGeoFitter, BetaGeoBetaBinomFitter
pd.options.display.max_columns = 50


class RegularizationAnalyzer:

    def __init__(self,
                 transaction_dataset: pd.DataFrame,
                 calibration_period_end: str,
                 observation_period_end: str,
                 id_column='SHOPPER_ID',
                 order_date_column='ORDER_DATE',
                 monetary_value_column='AGP_BUDGET_USD'):
        self.dataset = transaction_dataset
        self.calibration_period_end = calibration_period_end
        self.calibration_period_end_dt = pd.to_datetime(calibration_period_end,
                                                        format='%Y-%m-%d')
        self.observation_period_end = observation_period_end
        self.observation_period_end_dt = pd.to_datetime(observation_period_end,
                                                        format='%Y-%m-%d')
        self.id_column = id_column
        self.order_date_column = order_date_column
        self.monetary_value_column = monetary_value_column
        #
        # Calibration summary data with monetary value
        self.calibration_orders = self.dataset[self.dataset[order_date_column] <
                                          pd.to_datetime(calibration_period_end, format='%Y-%m-%d')]
        self.naive_monetary_value = self.calibration_orders[monetary_value_column].mean()
        print(f'naive monetary value is {self.naive_monetary_value}')
        self.calibration_summary = summary_data_from_transaction_data(transactions=self.calibration_orders,
                                                                      customer_id_col=id_column,
                                                                      datetime_col=order_date_column,
                                                                      monetary_value_col=monetary_value_column,
                                                                      datetime_format='%Y-%m-%d',
                                                                      observation_period_end=observation_period_end,
                                                                      freq='D',
                                                                      freq_multiplier=1,
                                                                      include_first_transaction=False)
        #
        # Calibration period: collapse into summary which is used to fit model and estimate future values
        #
        self.returning_customers = self.calibration_summary[self.calibration_summary.frequency > 0]
        #
        # Holdout period: we keep distinct orders to analyze conditional profit
        #
        self.holdout_orders_df = transaction_dataset[self.dataset[order_date_column].between(self.calibration_period_end_dt,
                                                                                             self.observation_period_end_dt)]

        self.holdout_shoppers_df = pd.DataFrame(self.holdout_orders_df[id_column].unique(), columns=[id_column])
        calibration_summary_temp = self.calibration_summary.reset_index()
        calibration_summary_temp.rename(columns={'id': id_column}, inplace=True)
        #
        # some holdout shoppers were not known in the calibration period
        # we will drop those records -- they could not have been scored
        self.holdout_shoppers_df = self.holdout_shoppers_df.merge(calibration_summary_temp[[id_column,
                                                                                            'frequency',
                                                                                            'monetary_value']],
                                                                  how='inner')
        # holdout_shoppers_df only contains those which were *known* in the calibration period
        # therefore we use an inner merge and expect this will drop all previously unknown shoppers
        self.holdout_orders_df = self.holdout_orders_df.merge(self.holdout_shoppers_df[[self.id_column]],
                                                              how='inner')
        # while we do not mind new columns getting added we have to ensure no records
        # are dropped in functions.  These two variables can keep us honest with assertions
        self.holdout_shopper_count = len(self.holdout_shoppers_df)
        self.holdout_order_count = len(self.holdout_orders_df)

    def gg_fit(self, penalizer_coefficient: float):
        """
        Fits gamma-gamma model with a penalizer coefficient
        :param penalizer_coefficient:
        :return: fit model, dictionary of gamma-gamma parameters from underlying model (p,q,v)
        """
        ggf = GammaGammaFitter(penalizer_coef=penalizer_coefficient)
        fit_results = ggf.fit(self.returning_customers.frequency,
                              self.returning_customers.monetary_value)
        print(f'Fit results for penalizer {penalizer_coefficient} are {fit_results}')
        return ggf, fit_results.params_

    @staticmethod
    def get_repeat_model_types():
        """
        Return: Possible model types for BGF
        """
        return ['BetaGeo', 'ModifiedBetaGeo', 'BetaGeoBetaBinom', 'ParetoNBD']

    def bg_fit(self, penalizer_coefficient: float, model_type: str = 'ModifiedBetaGeo'):
        """
        Fits beta-gamma model with a penalizer coefficient

        :param penalizer_coefficient: applied to model
        :param model_type: type of model to fit
        :return: fit model, dictionary of parameters from underlying model (r, a, b, alpha)
        """
        assert model_type == 'ModifiedBetaGeo', 'Only ModifiedBetaGeoFit supported -- to date'
        bgf = ModifiedBetaGeoFitter(penalizer_coef=penalizer_coefficient)
        fit_results = bgf.fit(self.calibration_summary.frequency,
                              self.calibration_summary.recency,
                              self.calibration_summary['T'])
        print(f'Fit results for penalizer {penalizer_coefficient} are {fit_results}')
        return bgf, fit_results.params_

    def bg_calculate_accuracy(self, fit_model, up_to_time_days: float):
        """
        Calculates accuracy for repeat models

        :param fit_model: fit model which will be applied.  Type could vary
        :param up_to_time_days: time in the future for parameters - this will be number of days as float
        :return:
        """
        assert isinstance(up_to_time_days, float), 'up_to_time_days should be type float'
        self.calibration_summary["holdout_order_prediction"] = fit_model.conditional_expected_number_of_purchases_up_to_time( \
            up_to_time_days,
            self.calibration_summary["frequency"],
            self.calibration_summary["recency"],
            self.calibration_summary["T"])

        # self.holdout_orders_df is all orders in the last period for shoppers known
        # in the calibration period.  So every record should merge properly
        self.holdout_orders_per_shopper = self.holdout_orders_df.groupby([self.id_column])[self.monetary_value_column].count().reset_index()
        self.holdout_orders_per_shopper.rename(columns={self.monetary_value_column: 'holdout_order_count'}, inplace=True)
        if 'holdout_order_count' in self.calibration_summary:
            self.calibration_summary.drop(columns=['holdout_order_count'], inplace=True)
        self.calibration_summary = self.calibration_summary.merge(self.holdout_orders_per_shopper, on=self.id_column, how='left')
        self.calibration_summary.loc[self.calibration_summary.holdout_order_count.isnull() == True,
                                     'holdout_order_count'] = 0

        # naive model assumes no orders - so naive absolute error is the sum of actual orders
        self.calibration_summary['naive_absolute_error'] = self.calibration_summary.holdout_order_count
        self.calibration_summary['naive_square_absolute_error'] = \
            self.calibration_summary.naive_absolute_error.pow(2)
        self.calibration_summary['absolute_error'] = \
            (self.calibration_summary.holdout_order_count - self.calibration_summary.holdout_order_prediction).abs()
        self.calibration_summary['square_absolute_error'] = \
            self.calibration_summary.absolute_error.pow(2)

        metrics = {
            'r': fit_model.params_['r'],
            'a': fit_model.params_['a'],
            'b': fit_model.params_['b'],
            'alpha': fit_model.params_['alpha'],
            'naive_mae': self.calibration_summary.naive_absolute_error.mean(),
            'naive_mse': self.calibration_summary.naive_square_absolute_error.mean(),
            'naive_rmse': np.sqrt(self.calibration_summary.naive_square_absolute_error.mean()),
            'mae': self.calibration_summary.absolute_error.mean(),
            'mse': self.calibration_summary.square_absolute_error.mean(),
            'rmse': np.sqrt(self.calibration_summary.square_absolute_error.mean()),
            'predicted_orders': self.calibration_summary.holdout_order_prediction.sum(),
            'actual_orders': self.calibration_summary.holdout_order_count.sum(),
            'predicted_orders_per_customer': self.calibration_summary.holdout_order_prediction.mean(),
            'actual_orders_per_customer': self.calibration_summary.holdout_order_count.mean(),
            'RSpearman': self.calibration_summary[['holdout_order_count',
                                                   'holdout_order_prediction']].corr(method='spearman').loc['holdout_order_count',
                                                                                                            'holdout_order_prediction']
        }

        assert self.holdout_shopper_count == len(self.holdout_shoppers_df), \
            'coding error: length of holdout shopper count changed'
        assert self.holdout_order_count == len(self.holdout_orders_df), \
            'coding error: length of holdout order count changed'
        return metrics, self.calibration_summary, self.holdout_orders_per_shopper


    def gg_calculate_accuracy(self, gg_parameters):
        """
        calculate the loss for observation period given gg parameters
        :param gg_parameters:
        :return: tuple - metrics, result set for orders, assumptions set per shopper
        """
        p = gg_parameters['p']
        q = gg_parameters['q']
        v = gg_parameters['v']

        # for every shopper we can calculate their profit
        population_mean = v * p / (q - 1)
        self.holdout_shoppers_df['individual_weight'] = \
            p * self.holdout_shoppers_df.frequency / (p * self.holdout_shoppers_df.frequency + q - 1)
        self.holdout_shoppers_df['conditional_exp_profit'] = \
            (1 - self.holdout_shoppers_df.individual_weight) * population_mean + \
            self.holdout_shoppers_df.individual_weight * self.holdout_shoppers_df.monetary_value

        if 'conditional_exp_profit' in self.holdout_orders_df.columns:
            self.holdout_orders_df.drop(columns=['conditional_exp_profit'], inplace=True)

        # no new records should be dropped because the holdout orders are set in constructor to only
        # have previously known shoppers
        self.holdout_orders_df = self.holdout_orders_df.merge(self.holdout_shoppers_df[[self.id_column,
                                                                                        'conditional_exp_profit']],
                                                              how='inner')
        print(f'gamma-gamma computed population mean is {population_mean}')
        if population_mean < 0:
            print('ATTENTION: negative population mean is very bad')
            print(f'p: {p}, q: {q}, v: {v}')
        self.holdout_orders_df['naive_absolute_error'] = \
            (self.holdout_orders_df[self.monetary_value_column] - self.naive_monetary_value).abs()
        self.holdout_orders_df['naive_square_absolute_error'] = \
            self.holdout_orders_df.naive_absolute_error.pow(2)
        self.holdout_orders_df['absolute_error'] = \
            (self.holdout_orders_df[self.monetary_value_column] - self.holdout_orders_df.conditional_exp_profit).abs()
        self.holdout_orders_df['square_absolute_error'] = \
            self.holdout_orders_df.absolute_error.pow(2)

        metrics = {
            'p': p,
            'q': q,
            'v': v,
            'population_mean': population_mean,
            'naive_mae': self.holdout_orders_df.naive_absolute_error.mean(),
            'naive_mse': self.holdout_orders_df.naive_square_absolute_error.mean(),
            'naive_rmse': np.sqrt(self.holdout_orders_df.naive_square_absolute_error.mean()),
            'mae': self.holdout_orders_df.absolute_error.mean(),
            'mse': self.holdout_orders_df.square_absolute_error.mean(),
            'rmse': np.sqrt(self.holdout_orders_df.square_absolute_error.mean()),
            'predicted_spend': self.holdout_orders_df.conditional_exp_profit.sum(),
            'actual_spend': self.holdout_orders_df[self.monetary_value_column].sum(),
        }

        assert self.holdout_shopper_count == len(self.holdout_shoppers_df), \
            'coding error: length of holdout shopper count changed'
        assert self.holdout_order_count == len(self.holdout_orders_df), \
            'coding error: length of holdout order count changed'
        return metrics, self.holdout_orders_df, self.holdout_shoppers_df

    @staticmethod
    def bg_plot_metrics(df, country, time_period, model_type='ModifiedBetaGeo', log_shift=.00000001):
        """
        Plots MAE and RMSE charts for the data and labels it for a country

        :param df: result dataframe which has naive_rmse, naive_mae, mae, and rmse per penalty
        :param country: country for labelling the plot
        :param time_period: for labelling the plot
        :param model_type: type of model being described
        :param log_shift: amount to shift penalty by in order to allow plotting of zero and keeping values ordered
        :return: None
        """
        assert model_type in RegularizationAnalyzer.get_repeat_model_types(), \
            f'Model type {model_type} unrecognized'
        # using log for plotting, but we have to shift over due to log(0)=-inf
        df['log_penalty'] = np.log(df.penalty+log_shift)
        log_shift = np.log(log_shift)

        # RMSE chart
        rmse_min = df.rmse.min()
        best_df = df[df.rmse == rmse_min]
        naive_rmse = df.naive_rmse.min()
        ax = df.plot('log_penalty', 'rmse', marker='o', markersize=3)
        best_df.plot.scatter('log_penalty', 'rmse', c='xkcd:green', marker='o', s=30, ax=ax)
        ax.annotate(f'Optimal at {best_df.penalty.min()}: {"{:0.3f}".format(rmse_min)}', xy=(best_df.log_penalty.min(), 0.95*rmse_min))
        plt.axhline(y=naive_rmse, color='black', linestyle='--')
        ax.annotate(f'Naive guess assuming no reorders', fontsize=9, xy=(log_shift, naive_rmse))
        ax.set_title(f'RMSE {model_type} for {country} {time_period}', fontsize=11)
        ax.annotate('RMSE: regularization parameter v. naive model', xy=(log_shift, 0))
        ax.annotate('R=0', fontsize=9, xy=(log_shift, 1.02 * df[df.penalty == 0].rmse.min()))
        ax.set_ylim(0, naive_rmse*1.8)
        plt.show()

        # MAE chart
        mae_min = df.mae.min()
        best_df = df[df.mae == mae_min]
        naive_mae = df.naive_mae.min()
        ax = df.plot('log_penalty', 'mae', marker='o', markersize=3)
        best_df.plot.scatter('log_penalty', 'mae', c='xkcd:green', marker='o', s=30, ax=ax)
        ax.annotate(f'Optimal at {best_df.penalty.min()}: {"{:0.3f}".format(mae_min)}', xy=(best_df.log_penalty.min(), 0.95 * mae_min))
        plt.axhline(y=naive_mae, color='black', linestyle='--')
        ax.annotate(f'Naive guess assuming no reorders', fontsize=9, xy=(log_shift, naive_mae))
        ax.set_title(f'MAE {model_type} for {country} {time_period}', fontsize=11)
        ax.annotate('MAE: regularization parameter v. naive model', xy=(log_shift, 0))
        ax.set_ylim(0, naive_mae * 1.8)
        ax.annotate('R=0', fontsize=9, xy=(log_shift, 1.02 * df[df.penalty == 0].mae.min()))
        plt.show()

        # RSpearman chart
        rspearman_max = df.RSpearman.max()
        best_df = df[df.RSpearman == rspearman_max]
        naive_spearman = 0.0
        ax = df.plot('log_penalty', 'RSpearman', marker='o', markersize=3)
        best_df.plot.scatter('log_penalty', 'RSpearman', c='xkcd:green', marker='o', s=30, ax=ax)
        ax.annotate(f'Optimal at {best_df.penalty.min()}: {"{:0.3f}".format(rspearman_max)}',
                    xy=(best_df.log_penalty.min(), 0.95 * rspearman_max))
        plt.axhline(y=naive_spearman, color='black', linestyle='--')
        ax.annotate(f'Naive guess assuming no reorders', fontsize=9, xy=(log_shift, naive_spearman))
        ax.set_title(f'RSpearman {model_type} for {country} {time_period}', fontsize=11)
        # adjusted position of this title line to avoid overlap with naive guess
        ax.annotate('RSpearman (max): regularization parameter v. naive model', xy=(log_shift, 0.04))
        ax.annotate('R=0', fontsize=9, xy=(log_shift, 1.02 * df[df.penalty == 0].RSpearman.max()))
        plt.show()

    @staticmethod
    def gg_plot_metrics(df, country, time_period, log_shift=.00000001):
        """
        Plots MAE and RMSE charts for the data and labels it for a country

        :param df: result dataframe which has naive_rmse, naive_mae, mae, and rmse per penalty
        :param country: country for labelling the plot
        :param time_period: for labelling the plot
        :param log_shift: amount to shift penalty by in order to allow plotting of zero and keeping values ordered
        :return: None
        """

        # using log for plotting, but we have to shift over due to log(0)=-inf
        df['log_penalty'] = np.log(df.penalty + log_shift)
        log_shift = np.log(log_shift)

        bad_df = df[df.population_mean < 0]

        # RMSE chart
        rmse_min = df.rmse.min()
        best_df = df[df.rmse == rmse_min]
        naive_rmse = df.naive_rmse.min()
        ax = df.plot('log_penalty', 'rmse', marker='o', markersize=3)
        bad_df.plot.scatter('log_penalty', 'rmse', c='xkcd:red', marker='x', s=20, ax=ax)
        best_df.plot.scatter('log_penalty', 'rmse', c='xkcd:green', marker='o', s=30, ax=ax)
        ax.annotate(f'Optimal at {best_df.penalty.min()}: {"{:0.3f}".format(rmse_min)}', xy=(best_df.log_penalty.min(), 0.95*rmse_min))
        plt.axhline(y=naive_rmse, color='black', linestyle='--')
        ax.annotate(f'Naive guess using mean spend', fontsize=9, xy=(log_shift, naive_rmse))
        ax.set_title(f'RMSE Gamma-Gamma for {country} {time_period}', fontsize=11)
        ax.annotate('RMSE: regularization parameter v. naive model', xy=(log_shift, 0))
        ax.annotate('R=0', xy=(log_shift, 1.05*df[df.penalty == 0].rmse.min()))
        ax.set_ylim(0, naive_rmse*1.8)
        plt.show()

        # MAE chart
        mae_min = df.mae.min()
        best_df = df[df.mae == mae_min]
        naive_mae = df.naive_mae.min()
        ax = df.plot('log_penalty', 'mae', marker='o', markersize=3)
        bad_df.plot.scatter('log_penalty', 'mae', c='xkcd:red', marker='X', s=20, ax=ax)
        best_df.plot.scatter('log_penalty', 'mae', c='xkcd:green', marker='o', s=30, ax=ax)
        ax.annotate(f'Optimal at {best_df.penalty.min()}: {"{:0.3f}".format(mae_min)}', xy=(best_df.log_penalty.min(), 0.95*mae_min))
        plt.axhline(y=naive_mae, color='black', linestyle='--')
        ax.annotate(f'Naive guess using mean spend', fontsize=9, xy=(log_shift, naive_mae))
        ax.set_title(f'MAE Gamma-Gamma for {country} {time_period}', fontsize=11)
        ax.annotate('MAE: regularization parameter v. naive model', xy=(log_shift, 0))
        ax.set_ylim(0, naive_mae*1.8)
        ax.annotate('R=0', xy=(log_shift, 1.05*df[df.penalty == 0].mae.min()))
        plt.show()


def conditional_profit_optimization(optimizer, time_period):
    """
    conditional profit (gamma-gamma) model accuracy

    :param optimizer: optimizer which has data set loaded
    :param time_period: string to describe time period
    :return: none
    """
    accuracies = {}
    results = {}
    assumptions = {}
    # gg model prefers extremely small values, and it distorts significantly as 0.001 and larger are used
    penalties = [0, 0.0000001, 0.000001, 0.000005, 0.00001, 0.00005, 0.0001, 0.001, 0.01, 0.02]
    for penalty in tqdm(penalties):
        _, params = optimizer.gg_fit(penalty)
        accuracies[penalty], results[penalty], assumptions[penalty] = opt.gg_calculate_accuracy(params)

    # plot the accuracies
    data_rows = []
    field_list = ['penalty', 'p', 'q', 'v', 'population_mean',
                  'predicted_spend', 'actual_spend',
                  'naive_mae', 'naive_mse', 'naive_rmse', 'mae', 'mse', 'rmse']
    for x in penalties:
        new_row = [x]
        for field in field_list[1:]:
            new_row.append(accuracies[x][field])
        data_rows.append(new_row)
    datasets[country] = pd.DataFrame(data_rows, columns=field_list)
    print('conditional profit results')
    print(country)
    print(datasets[country])
    RegularizationAnalyzer.gg_plot_metrics(datasets[country], country, time_period)
    return accuracies, results, assumptions


def probability_repeat_optimization(optimizer, time_period, holdout_duration_days):
    """
    repeat model accuracy

    :param optimizer: optimizer which has data set loaded
    :param time_period: string to describe time period
    :return: none
    """
    accuracies = {}
    results = {}
    holdout_orders = {}
    # smaller values did not have any real effect -- unlike the gg model
    penalties = [0, 0.000001,  0.00001,  0.0001, 0.001, 0.01, 0.1, 1, 10, 100]
    for penalty in tqdm(penalties):
        bg_fit, bg_params = optimizer.bg_fit(penalty)
        accuracies[penalty], results[penalty], holdout_orders[penalty] = \
            optimizer.bg_calculate_accuracy(bg_fit, holdout_duration_days)
        print(accuracies[penalty])

    # plot the accuracies
    data_rows = []
    for x in penalties:
        new_row = [x]
        field_list = ['penalty', 'a', 'b', 'alpha', 'r', 'predicted_orders', 'actual_orders',
                      'naive_mae', 'naive_mse', 'naive_rmse', 'mae', 'rmse', 'predicted_orders_per_customer',
                      'actual_orders_per_customer', 'RSpearman']
        for field in field_list[1:]:
            new_row.append(accuracies[x][field])
        data_rows.append(new_row)
    datasets[country] = pd.DataFrame(data_rows, columns=field_list)
    print('probability repeat results')
    print(country)
    print(datasets[country])
    RegularizationAnalyzer.bg_plot_metrics(datasets[country], country, time_period)
    return accuracies, results, holdout_orders


#
# - Data
#

src = os.path.join('..', '..', 'data', '01_raw',
                   'VPLifetimes--orders--IE-ES--2010-07-01-2020-07-21--2010-07-01-2020-07-21.0.11.pkl')
all_orders_df = pd.read_pickle(src)
all_orders_df.loc[all_orders_df.AGP_BUDGET_USD <= 0, 'AGP_BUDGET_USD'] = 0.01
all_orders_df.AGP_BUDGET_USD = all_orders_df.AGP_BUDGET_USD.astype('float64')

#
# - TEMPORARY OPTIMIZATION: trim data set to 2019+ to save time and run through code
#

# Debugging tool: uncomment next line if smaller data set needed.  Keep an eye on calibration and observation periods
# all_orders_df = all_orders_df[all_orders_df.ORDER_DATE > pd.to_datetime('2018-01-01', format='%Y-%m-%d')]

datasets = {}
calibration_end = '2019-06-30'
observation_end = '2020-07-01'
calibration_end_dt = datetime.strptime(calibration_end, "%Y-%m-%d").date()
observation_end_dt = datetime.strptime(observation_end, "%Y-%m-%d").date()
holdout_duration_days = (observation_end_dt - calibration_end_dt).days
print('Testing with calibration end {calibration_end} and observation end {observation_end}')
print(f'Holdout duration {holdout_duration_days}')
for country in ['IE', 'ES']:  # quicker country first in case we do break out later
    orders_df = all_orders_df[all_orders_df.ACCOUNT_CREATION_COUNTRY == country]
    # holdout the entire last year
    opt = RegularizationAnalyzer(orders_df, calibration_end, observation_end)
    time_period = '2010/7-2019/6 train; 2019/7-2020-6 test'

    if True:  # modified-beta-geo
        accuracies, results, holdout_orders = probability_repeat_optimization(opt,
                                                                              time_period,
                                                                              1.0 * holdout_duration_days)

    if True:  # gamma-gamma conditional profit optimization
        accuracies, results, assumptions = conditional_profit_optimization(opt, time_period)

    if False:  # for debugging may skip out after one country
        print('breaking early')
        break
