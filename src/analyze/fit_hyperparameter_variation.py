import os
import logging
import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import ParameterGrid
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis
pd.options.display.max_columns = 50

log_dir = os.path.join('..', '..', 'logs', 'temp')
log_filename = os.path.join(log_dir, 'fit-hyperparameter-variatsion.log')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
if os.path.exists(log_filename):
    os.remove(log_filename)
logging.basicConfig(filename=log_filename,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger('__name__')

#
# Fit data and store descriptor of the case and resulting parameters
#
# Analysis is separate
#
src = os.path.join('..', '..', 'data', '01_raw',
                   'VPLifetimes--orders--IE-ES--2010-07-01-2020-07-21--2010-07-01-2020-07-21.0.11.pkl')
orders_df = pd.read_pickle(src)
orders_df['order_date_dt'] = pd.to_datetime(orders_df.ORDER_DATE, format='%Y-%m-%d')
model_type = 'ModifiedBetaGeoFit'

#
# This version is faster than all combinations with each other, which is unnecessary
# especially as it seems that we will use regularizer=0.0 based on early research
# The two models are independently fitted in either case.
grid1 = {
    'bg_coefficient': [0],
    'gg_coefficient': [0, 0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.05]
}
grid2 = {
    'bg_coefficient': [0, 0.0001, 0.001, 0.01, 0.1, 0.2, 0.5],
    'gg_coefficient': [0]
}
model_results = []
orders_df.info()
start_date = '2010-07-01'
end_date = '2020-07-01'
start_date_dt = pd.to_datetime(start_date, format='%Y-%m-%d')
for grid in tqdm([grid1, grid2]):
    for parameters in tqdm(ParameterGrid(grid)):
        for country in ['IE', 'ES']:
            bg_penalizer = parameters['bg_coefficient']
            gg_penalizer = parameters['gg_coefficient']
            cohort = f'{country}-H:{bg_penalizer}-{gg_penalizer}'
            analyze_orders_df = orders_df[orders_df.ACCOUNT_CREATION_COUNTRY == country]
            analyze_orders_df = analyze_orders_df[analyze_orders_df.order_date_dt > start_date_dt]
            analyze_orders_df = analyze_orders_df[analyze_orders_df.FIRST_ORDER_DATETIME > start_date_dt]
            vp_lifetime_analysis = VPLifetimesAnalysis(VPLifetimesData(analyze_orders_df))
            results, _, _ = vp_lifetime_analysis.fit_order_data(end_date,
                                                                bg_penalizer,
                                                                gg_penalizer=gg_penalizer)
            results['country'] = country
            results['cohort'] = cohort
            results['start_date'] = start_date
            results['end_date'] = end_date
            results['seasonal'] = ''
            results['sample_percent'] = 100
            model_results.append(results)

results_df = pd.DataFrame.from_dict(model_results)

# one time population of data set from results
results_df.to_csv(os.path.join('..', '..', 'data', '09_test_data', 'test_hyperparameters_2010-2020.csv'))
