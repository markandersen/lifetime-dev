from src.lifetime_dev.VPLifetimesHoldoutPlots import VPLifetimesHoldoutPlots
import os
import pandas as pd
from tqdm import tqdm
pd.options.display.max_columns = 50
#
# Sample data
#
# hard-coded for MGB at present

datasets = [
#    ('normal', 'Differing Calibration Dates', 'test_IE_2020-07-17-1.csv'),
#    ('normal', 'Differing 70% Random Samples', 'test_ES-2020-07-17-1.csv'),
#    ('normal', '2010 to 2020 Comparisons', 'test_2015-2020.csv')
    ('repetitive', 'Beta Regularization Plot', 'test_hyperparameters_2010-2020.csv')
]

plot_filename = None

for set_type, cohort_descriptor, filename in tqdm(datasets):
    df = pd.read_csv(os.path.join('..', '..', 'data', '09_test_data', filename))

    if set_type == 'repetitive':
        # specific problems with data written
        df.cohort = df.cohort.str.replace('1e-06', '0.000001')
        df.cohort = df.cohort.str.replace('1e-05', '0.00001')
        # beta-cohort
        df[['_', 'beta_cohort', 'gamma_cohort']] = df.cohort.str.split('-', expand=True)
        df.cohort = df.beta_cohort


    # if cohort not filled in then end_date is better
    df.loc[df.cohort == '', 'cohort'] = df.end_date

    # repetitive means that the beta columns are not unique
    # safe operation for all sets
    temp_df = df[['country', 'cohort', 'b-r', 'b-alpha', 'b-a', 'b-b']].drop_duplicates()
    df = temp_df

    # divide by country
    country_list = df.country.unique().tolist()
    for country in country_list:
        print(f'Country: {country}')
        plot_title = f'{country} - {cohort_descriptor}'

        country_df = df[df.country == country]
        #
        # Tests
        #

        # temporary max
        plot_filename = os.path.join('..', '..', 'data', '08_reporting',
                                     f'beta-sensitivity-{plot_title}.png'.replace(' ', '_'))
        VPLifetimesHoldoutPlots.plot_beta_sensitivity_sets(country_df,
                                                           'cohort',
                                                           suptitle_prefix=f'{country}: {cohort_descriptor}',
                                                           save_filename=plot_filename,
                                                           preferred_beta_label='H:0'
                                                           )


        parameter_df = pd.DataFrame(country_df, columns=['cohort', 'b-r', 'b-alpha', 'b-a', 'b-b'])

        plot_filename = f'beta-stability-{plot_title}.png'.replace(' ', '_')
        VPLifetimesHoldoutPlots.plot_parameter_stability_pairplot(parameter_df,
                                                                  'cohort',
                                                                  plot_title,
                                                                  plot_filename)

