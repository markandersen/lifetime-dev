import os
import pandas as pd
from tqdm import tqdm
from src.lifetime_dev.VPLifetimesData import VPLifetimesData
from src.lifetime_dev.VPLifetimesAnalysis import VPLifetimesAnalysis
pd.options.display.max_columns = 50


#
# Fit data and store descriptor of the case and resulting parameters
#
# Analysis separated

src = os.path.join('..', '..', 'data', '01_raw',
                   'VPLifetimes--orders--IE-ES--2010-07-01-2020-07-21--2010-07-01-2020-07-21.0.11.pkl')
orders_df = pd.read_pickle(src)
model_type = 'ModifiedBetaGeoFit'
orders_df['order_date_dt'] = pd.to_datetime(orders_df.ORDER_DATE, format='%Y-%m-%d')

model_results = []
for start_end in tqdm([
    ['2010-07-01', '2015-07-01'],
    ['2011-07-01', '2016-07-01'],
    ['2012-07-01', '2017-07-01'],
    ['2013-07-01', '2018-07-01'],
    ['2014-07-01', '2019-07-01'],
    ['2015-07-01', '2020-07-01'],
    ['2010-07-01', '2020-07-01'],
    ['2010-07-01', '2019-12-31'],
    ['2010-07-01', '2018-12-31']
]):
    start_date, end_date = start_end
    for country in ['IE', 'ES']:
        start_date_dt = pd.to_datetime(start_date, format='%Y-%m-%d')
        cohort = f'{country}-{start_date}-{end_date}'
        analyze_orders_df = orders_df[orders_df.ACCOUNT_CREATION_COUNTRY == country]
        analyze_orders_df = analyze_orders_df[analyze_orders_df.order_date_dt > start_date_dt]
        analyze_orders_df = analyze_orders_df[analyze_orders_df.FIRST_ORDER_DATETIME > start_date_dt]
        vp_lifetime_analysis = VPLifetimesAnalysis(VPLifetimesData(analyze_orders_df))
        results, _, _ = vp_lifetime_analysis.fit_order_data(end_date)
        results['country'] = country
        results['cohort'] = cohort
        results['start_date'] = start_date
        results['end_date'] = end_date
        results['seasonal'] = ''
        results['sample_percent'] = 100
        model_results.append(results)

results_df = pd.DataFrame.from_dict(model_results)

# one time population of data set from results
results_df.to_csv(os.path.join('..', '..', 'data', '09_test_data', 'varied-start-end-dates.csv')) # 'test_2015-2020.csv'))
